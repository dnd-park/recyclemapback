CREATE OR REPLACE FUNCTION trigger_set_timestamp()
    RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TABLE IF NOT EXISTS "points" (
      "id" bigserial not null primary key,
      "title" varchar not null,
      "description" varchar,
      "coordinates" geometry(Point, 4326) unique not null,
      "address" varchar not null,
      "city" int not null,
      "images" varchar[],
      "group" int,
      "fractions" int[] not null,
      "phones" varchar[],
      "sites" varchar[],
      "emails" varchar[],
      "status" varchar not null,
      "tags" varchar[],
      "created_at" timestamp with time zone default current_timestamp,
      "updated_at" timestamp with time zone default current_timestamp,
      "created_by" bigint not null,
      "updated_by" bigint not null
);

ALTER TABLE points ADD COLUMN IF NOT EXISTS grade numeric(5, 4) default 0;

CREATE OR REPLACE FUNCTION trigger_update_avg_grade()
    RETURNS TRIGGER AS $$
BEGIN UPDATE "points"
      SET "grade" = (select AVG("grades"."grade") FROM "grades" WHERE "point_id" = id) WHERE "id" = NEW.point_id;
      RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS set_timestamp_points ON "points";

CREATE TRIGGER set_timestamp_points
    BEFORE UPDATE ON "points"
    FOR EACH ROW
    WHEN (row_to_json(old)::jsonb - 'grade' is distinct from row_to_json(new)::jsonb - 'grade')
EXECUTE PROCEDURE trigger_set_timestamp();

CREATE UNIQUE INDEX IF NOT EXISTS idx_points_id ON points (id);
CREATE INDEX IF NOT EXISTS idx_points_city ON points(city);
CREATE INDEX IF NOT EXISTS idx_points_fractions ON points USING GIN (fractions);

CREATE TABLE IF NOT EXISTS "cities" (
      "id" bigserial not null primary key,
      "name" varchar
);

CREATE TABLE IF NOT EXISTS "groups" (
      "id" bigserial not null primary key,
      "name" varchar,
      "city" int
);

CREATE TABLE IF NOT EXISTS "users" (
     "id" bigserial not null primary key,
     "first_name" varchar,
     "second_name" varchar,
     "email" varchar unique,
     "password" varchar,
     "city" int,
     "phones" varchar[],
     "avatar" varchar,
     "user_type" varchar not null,
     "status" varchar not null,
     "created_at" timestamp with time zone default current_timestamp,
     "updated_at" timestamp with time zone default current_timestamp,
     "last_login" timestamp with time zone default current_timestamp,
     "vk_id" bigint,
     "expires_id" bigint,
     "access_token_vk" varchar
);

-- ALTER TABLE users ADD COLUMN IF NOT EXISTS vk_id bigint default 0;
-- ALTER TABLE users ADD COLUMN IF NOT EXISTS expires_id bigint default 0;
-- ALTER TABLE users ADD COLUMN IF NOT EXISTS access_token_vk varchar default '';

DROP TRIGGER IF EXISTS set_timestamp_users ON "users";

CREATE TRIGGER set_timestamp_users
    BEFORE UPDATE ON "users"
    FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

CREATE INDEX IF NOT EXISTS idx_users_type ON "users"(user_type);
CREATE INDEX IF NOT EXISTS idx_users_email ON "users"(email);

CREATE TABLE IF NOT EXISTS "timetables" (
      "id" bigserial not null primary key,
      "point_id" bigint not null,
      "is_round_the_clock" boolean not null,
      "is_break" boolean not null,
      "is_campaign" boolean not null,
      "is_floating" boolean not null,
      "monday" jsonb,
      "tuesday" jsonb,
      "wednesday" jsonb,
      "thursday" jsonb,
      "friday" jsonb,
      "saturday" jsonb,
      "sunday" jsonb,
      "break_start_time" timestamp,
      "break_end_time" timestamp,
      "action_start_time" timestamp,
      "action_end_time" timestamp
);

ALTER TABLE "timetables" DROP CONSTRAINT IF EXISTS timetables_point_id;
ALTER TABLE "timetables" ADD CONSTRAINT timetables_point_id UNIQUE ("point_id");

CREATE TABLE IF NOT EXISTS "comments" (
    "id" bigserial not null primary key,
    "comment" varchar not null,
    "point_id" bigint not null,
    "status" varchar not null,
    "created_by" int not null,
    "created_at" timestamp with time zone default current_timestamp,
    "updated_at" timestamp with time zone default current_timestamp
);

CREATE TABLE IF NOT EXISTS "requests" (
    "id" bigserial not null primary key,
    "point_id" bigint,
    "status" varchar not null,
    "title" varchar not null,
    "description" varchar,
    "coordinates" geometry not null,
    "address" varchar not null,
    "city" int not null,
    "images" varchar[],
    "group" int,
    "fractions" int[] not null,
    "phones" varchar[],
    "sites" varchar[],
    "emails" varchar[],
    "created_at" timestamp with time zone default current_timestamp,
    "updated_at" timestamp with time zone default current_timestamp,
    "created_by" bigint not null,
    "updated_by" bigint not null,
    "time" varchar
);

DROP TRIGGER IF EXISTS set_timestamp_requests ON "requests";

CREATE TRIGGER set_timestamp_requests
    BEFORE UPDATE ON "requests"
    FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();


CREATE TABLE IF NOT EXISTS "actions" (
       "id" bigserial not null primary key,
       "title" varchar,
       "action" varchar not null,
       "item_id" bigint,
       "item_type" varchar,
       "item_name" varchar,
       "actor_id" bigint not null,
       "actor_name" varchar not null,
       "city_id" bigint not null,
       "created_at" timestamp with time zone default current_timestamp
);

DROP TRIGGER IF EXISTS set_timestamp_requests ON "actions";

CREATE TRIGGER set_timestamp_requests
    BEFORE UPDATE ON "actions"
    FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

CREATE TABLE IF NOT EXISTS "complaints" (
      "id" bigserial not null primary key,
      author_id bigint not null,
      "email" varchar,
      "description" varchar not null,
      "point_id" bigint not null,
      "type" varchar not null,
      "responder_id" bigint not null default 0,
      "response" varchar not null default '',
      "is_answered" bool not null default false,
      "updated" timestamp not null default current_timestamp,
      "created" timestamp not null default current_timestamp
);

CREATE TABLE IF NOT EXISTS "grades" (
      "user_id" bigint,
      "point_id" bigint,
      "grade" int,
      unique ("user_id", "point_id")
);

DROP TRIGGER IF EXISTS set_avg_grades ON "grades";

CREATE TRIGGER set_avg_grades
    AFTER INSERT OR UPDATE OR DELETE ON "grades"
    FOR EACH ROW
EXECUTE PROCEDURE trigger_update_avg_grade();

CREATE TABLE IF NOT EXISTS "favorites" (
     "user_id" bigint,
     "point_id" bigint,
     unique ("user_id", "point_id")
);

ALTER TABLE "points" ADD FOREIGN KEY ("group") REFERENCES "groups" ("id");

ALTER TABLE "points" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "points" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "comments" ADD FOREIGN KEY ("point_id") REFERENCES "points" ("id");

ALTER TABLE "comments" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "actions" ADD FOREIGN KEY ("actor_id") REFERENCES "users" ("id");

ALTER TABLE "requests" ADD FOREIGN KEY ("point_id") REFERENCES "points" ("id");

ALTER TABLE "requests" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "requests" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "grades" ADD FOREIGN KEY ("point_id") REFERENCES "points" ("id");

ALTER TABLE "grades" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "favorites" ADD FOREIGN KEY ("point_id") REFERENCES "points" ("id");

ALTER TABLE "favorites" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "complaints" ADD FOREIGN KEY ("point_id") REFERENCES "points" ("id");

ALTER TABLE "complaints" ADD FOREIGN KEY ("author_id") REFERENCES "users" ("id");

ALTER TABLE "complaints" ADD FOREIGN KEY ("email") REFERENCES "users" ("email");

ALTER TABLE "timetables" ADD FOREIGN KEY ("point_id") REFERENCES "points" ("id");

ALTER TABLE "points" ADD FOREIGN KEY ("city") REFERENCES "cities" ("id");

-- ALTER TABLE "requests" ADD FOREIGN KEY ("city") REFERENCES "cities" ("id");

-- ALTER TABLE "users" ADD FOREIGN KEY ("city") REFERENCES "cities" ("id");

-- ALTER TABLE "groups" ADD FOREIGN KEY ("city") REFERENCES "cities" ("id");


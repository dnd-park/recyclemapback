package middleware

import (
	"context"
	"errors"
	"github.com/gorilla/sessions"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"gitlab.com/dnd-park/recyclemapback/internal/respond"
	"gitlab.com/dnd-park/recyclemapback/internal/usecases/user"
	"go.uber.org/zap"
	"net/http"
)

const (
	SessionName = "user-session"
)

type Middleware struct {
	sessionStore sessions.Store
	logger       *zap.SugaredLogger
	clientUrl    string
	userUC       user.UserUsecase
	responder    *respond.Responder
}

func NewMiddleware(logger *zap.SugaredLogger, clientUrl string, sessionStore sessions.Store, userU user.UserUsecase,
	responder *respond.Responder) Middleware {
	return Middleware{
		logger:       logger,
		clientUrl:    clientUrl,
		sessionStore: sessionStore,
		userUC:       userU,
		responder:    responder,
	}
}

func (m *Middleware) CORSMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Methods", "POST,PUT,DELETE,GET")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type, origin")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
		if r.Method == http.MethodOptions {
			m.responder.Respond(w, r, http.StatusOK, nil)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func (m *Middleware) AuthenticateUser(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, err := m.sessionStore.Get(r, SessionName)
		if err != nil {
			m.responder.Error(w, r, http.StatusUnauthorized, err)
			return
		}

		sessData, err := GetSessionVars(session)
		if err != nil {
			m.responder.Error(w, r, http.StatusUnauthorized, err)
			return
		}

		err = m.userUC.UpdateLastLogin(sessData.ID)
		if err != nil {
			m.responder.Error(w, r, http.StatusInternalServerError, err)
			return
		}

		next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), respond.CtxKeyUser, sessData)))
	})
}

func (m *Middleware) AdminMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, err := m.sessionStore.Get(r, SessionName)
		if err != nil {
			m.responder.Error(w, r, http.StatusUnauthorized, err)
			return
		}

		sessData, err := GetSessionVars(session)
		if err != nil {
			m.responder.Error(w, r, http.StatusUnauthorized, err)
			return
		}

		if sessData.UserType != model.AdminType && sessData.UserType != model.ModeratorType {
			m.responder.Error(w, r, http.StatusForbidden, errors.New("status forbidden"))
			return
		}

		next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), respond.CtxKeyUser, sessData)))
	})
}

func GetSessionVars(session *sessions.Session) (*model.SessionData, error) {
	var sessionData model.SessionData

	id, ok := session.Values["user_id"]
	if !ok {
		return nil, errors.New("unauthorized user_id")
	}

	city, ok := session.Values["city"]
	if !ok {
		return nil, errors.New("unauthorized city")
	}

	userType, ok := session.Values["user_type"]
	if !ok {
		return nil, errors.New("unauthorized user type")
	}

	email, ok := session.Values["email"]
	if !ok {
		return nil, errors.New("unauthorized email")
	}

	vkID, ok := session.Values["vk_id"]
	if !ok {
		return nil, errors.New("unauthorized")
	}

	name, ok := session.Values["user_name"]
	if !ok {
	}

	sessionData.ID = id.(int64)
	sessionData.City = city.(int64)
	sessionData.UserType = userType.(string)
	sessionData.Email = email.(string)
	sessionData.VkID = vkID.(int64)
	sessionData.Name = name.(string)

	return &sessionData, nil
}

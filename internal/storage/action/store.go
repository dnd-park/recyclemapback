package action

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

type Repository struct {
	db *pgxpool.Pool
}

func NewActionRepository(db *pgxpool.Pool) *Repository {
	return &Repository{db}
}

func (r *Repository) Create(actor *model.ActionItem, actions []model.Action) error {
	q, args, err := formatCreateQuery(actor, actions)
	if err != nil {
		return err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return err
	}
	defer rows.Close()

	_, err = scanIds(rows)
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) List(params model.ActionParams) ([]model.Action, error) {
	q, args, err := formatListQuery(params)
	if err != nil {
		return nil, err
	}
	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	return scanActions(rows)
}

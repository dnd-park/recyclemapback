package action

import (
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v4"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

var qb = sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

const (
	table  = `actions`
	fields = `"action", 
		"title", 
		"item_id", 
		"item_type", 
		"item_name", 
		"actor_id",
		"actor_name",
		"city_id"`
	scanFields = fields + `, "id", "created_at"`
)

func scanIds(rows pgx.Rows) ([]int64, error) {
	ids := make([]int64, 0)
	for rows.Next() {
		var id int64
		if err := rows.Scan(&id); err != nil {
			return nil, err
		}

		ids = append(ids, id)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return ids, nil
}

func scanActions(rows pgx.Rows) ([]model.Action, error) {
	actions := make([]model.Action, 0)
	for rows.Next() {
		var action model.Action
		if err := rows.Scan(&action.Action, &action.Title,
			&action.Item.ID, &action.Item.Type, &action.Item.Name,
			&action.Actor.ID, &action.Actor.Name,
			&action.City, &action.ID, &action.CreatedAt); err != nil {
			return nil, err
		}

		actions = append(actions, action)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return actions, nil
}

func formatCreateQuery(actor *model.ActionItem, actions []model.Action) (string, []interface{}, error) {
	query := qb.
		Insert(table).
		Columns(fields)

	for _, a := range actions {
		query = query.Values(a.Action, a.Title,
			a.Item.ID, a.Item.Type, a.Item.Name,
			actor.ID, actor.Name, a.City)
	}

	return query.ToSql()
}

func formatListQuery(params model.ActionParams) (string, []interface{}, error) {
	query := qb.
		Select(scanFields).
		From(table)

	if len(params.Items) != 0 {
		query = query.Where(sq.Eq{"item_id": params.Items})
	}

	if len(params.Actors) != 0 {
		query = query.Where(sq.Eq{"actor_id": params.Actors})
	}

	if len(params.Types) != 0 {
		query = query.Where(sq.Eq{"item_type": params.Types})
	}

	if len(params.Actions) != 0 {
		query = query.Where(sq.Eq{"action": params.Actions})
	}

	if !params.StartDate.IsZero() && !params.EndDate.IsZero() {
		query = query.Where(sq.And{sq.Expr("created_at > ?", params.StartDate), sq.Expr("created_at < ?", params.EndDate)})
	}

	if params.Desc {
		query = query.OrderBy("created_at desc")
	} else {
		query = query.OrderBy("created_at asc")
	}

	if params.Limit > 0 && params.Page > 0 {
		query = query.Offset(params.Limit * (params.Page - 1)).Limit(params.Limit)
	}

	return query.ToSql()
}

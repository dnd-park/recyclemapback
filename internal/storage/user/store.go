package user

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

type Repository struct {
	db *pgxpool.Pool
}

func NewUserRepository(db *pgxpool.Pool) *Repository {
	return &Repository{db}
}

func (r *Repository) Create(user *model.User) error {
	q, args, err := formatCreateQuery(*user)
	if err != nil {
		return err
	}

	err = r.db.QueryRow(context.Background(), q, args...).
		Scan(&user.ID)
	if err != nil {
		return err
	}
	return nil
}

func (r *Repository) Get(field string, data interface{}) ([]model.User, error) {
	// TODO: нужны ли тут доп проверки
	q, args, err := formatGetQuery(field, data)
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users, err := scanUsers(rows)
	if err != nil {
		return nil, err
	}

	return users, nil
}

func (r *Repository) Update(user *model.User) error {
	q, args, err := formatUpdateQuery(*user)
	if err != nil {
		return err
	}

	_, err = r.db.Exec(context.Background(), q, args...)
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) UpdateStatus(id int64, newType string, cityID int64) error {
	q, args, err := formatUpdateStatusQuery(id, newType, cityID)
	if err != nil {
		return err
	}

	_, err = r.db.Exec(context.Background(), q, args...)
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) UpdateLastLogin(id int64) error {
	q, args, err := formatUpdateLastLoginQuery(id)
	if err != nil {
		return err
	}

	_, err = r.db.Exec(context.Background(), q, args...)
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) List(params *model.UserFilter) ([]model.User, error) {
	q, args, err := formatListQuery(params)
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users, err := scanUsers(rows)
	if err != nil {
		return nil, err
	}

	return users, nil
}

//
func (r *Repository) CreateVkUser(us *model.VkUser) (int64, error) {
	var id int64
	q, args, err := formatCreateVkQuery(us)
	if err != nil {
		return 0, err
	}

	err = r.db.QueryRow(context.Background(), q, args...).
		Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (r *Repository) LinkVkToEmail(us *model.VkUser) (int64, error) {
	var id int64
	q, args, err := formatLinkVkQuery(us)
	if err != nil {
		return 0, err
	}

	err = r.db.QueryRow(context.Background(), q, args...).
		Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

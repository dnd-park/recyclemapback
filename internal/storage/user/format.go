package user

import (
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v4"
	"github.com/lib/pq"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"time"
)

var qb = sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

const (
	table  = `users`
	fields = `"first_name",
     "second_name",
     "email",
     "city",
     "phones",
     "avatar",
     "user_type",
     "status"`
	scanFields      = `"id",` + fields + `, "password", "vk_id"` + "," + timeFields
	timeFields      = `"created_at", "updated_at", "last_login" `
	insertFields    = fields + `, "password"`
	vkFields        = fields + `, "vk_id", "expires_vk", "access_token_vk"`
	returningFields = ` RETURNING "id"`
)

func scanUsers(rows pgx.Rows) ([]model.User, error) {
	var tempEmail interface{}
	var tempPassword interface{}
	var tempVkID interface{}
	users := make([]model.User, 0)
	for rows.Next() {
		var user model.User
		var phones pq.StringArray
		if err := rows.Scan(&user.ID, &user.FirstName, &user.SecondName, &tempEmail, &user.City, &phones,
			&user.Avatar, &user.UserType, &user.Status, &tempPassword, &tempVkID, &user.RegistrationDate,
			&user.LastUpdated, &user.LastLogin); err != nil {
		}

		if tempEmail != nil {
			user.Email = tempEmail.(string)
		}

		if tempPassword != nil {
			user.EncryptPassword = tempPassword.(string)
		}

		if tempVkID != nil {
			user.VkID = tempVkID.(int64)
		}

		user.Phones = phones
		users = append(users, user)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return users, nil
}

func formatCreateQuery(user model.User) (string, []interface{}, error) {
	query := qb.
		Insert(table).
		Columns(insertFields).
		Suffix(returningFields)

	query = query.Values(user.FirstName, user.SecondName, user.Email, user.City,
		pq.Array(user.Phones), user.Avatar, user.UserType, user.Status, user.EncryptPassword)

	return query.ToSql()
}

func formatGetQuery(field string, data interface{}) (string, []interface{}, error) {
	return qb.
		Select(scanFields).
		From(table).
		Where(sq.Eq{field: data}).
		ToSql()
}

func formatUpdateQuery(user model.User) (string, []interface{}, error) {
	return qb.Update(table).
		Set("first_name", user.FirstName).
		Set("second_name", user.SecondName).
		Set("city", user.City).
		Set("phones", pq.Array(user.Phones)).
		Set("avatar", user.Avatar).
		Set("user_type", user.UserType).
		Set("status", user.Status).
		Where(sq.Eq{"id": user.ID}).
		ToSql()
}

func formatUpdateStatusQuery(id int64, newType string, cityID int64) (string, []interface{}, error) {
	query := qb.Update(table).
		Set("user_type", newType)

	if cityID > 0 {
		query = query.Set("city", cityID)
	}
	return query.Where(sq.Eq{"id": id}).
		ToSql()
}

func formatUpdateLastLoginQuery(id int64) (string, []interface{}, error) {
	query := qb.Update(table).
		Set("last_login", time.Now())

	return query.Where(sq.Eq{"id": id}).
		ToSql()
}

func formatListQuery(params *model.UserFilter) (string, []interface{}, error) {
	query := qb.
		Select(scanFields).
		From(table)

	if params.City != 0 {
		query = query.Where(sq.Eq{"city": params.City})
	}

	if params.Status != "" {
		query = query.Where(sq.Eq{"status": params.Status})
	}

	if params.Types != nil {
		query = query.Where(sq.Eq{"user_type": params.Types})
	}

	return query.
		OrderBy("last_login desc").
		Offset(params.Limit * (params.Page - 1)).
		Limit(params.Limit).
		ToSql()
}

func formatCreateVkQuery(user *model.VkUser) (string, []interface{}, error) {
	var email interface{}
	if user.Email != "" {
		email = user.Email
	} else {
		email = nil
	}

	query := qb.
		Insert(table).
		Columns(vkFields).
		Suffix(returningFields)

	query = query.Values(user.FirstName, user.LastName, email, 0,
		nil, user.Avatar, model.UserType, model.ActiveStatus, user.VkID, user.ExpiresIn, user.AccessToken)

	return query.ToSql()
}

func formatLinkVkQuery(user *model.VkUser) (string, []interface{}, error) {
	return qb.Update(table).
		Set("first_name", user.FirstName).
		Set("second_name", user.LastName).
		Set("avatar", user.Avatar).
		Set("vk_id", user.VkID).
		Set("access_token_vk", user.AccessToken).
		Set("expires_vk", user.ExpiresIn).
		Where(sq.Eq{"email": user.Email}).
		Suffix(returningFields).
		ToSql()
}

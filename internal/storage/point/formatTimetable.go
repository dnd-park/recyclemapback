package point

import (
	sq "github.com/Masterminds/squirrel"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

var qbT = sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

const (
	tableT               = `timetables`
	fieldsWithoutPointId = `"is_round_the_clock",
     "is_break",
     "is_campaign",
	 "is_floating",
	 "monday",
	 "tuesday",
	 "wednesday",
	 "thursday",
	 "friday",
	 "saturday",
	 "sunday",
     "break_start_time",
     "break_end_time",
     "action_start_time", 
     "action_end_time"`
	fieldsT = `"point_id", ` + fieldsWithoutPointId
)

func formatCreateTimetablesQuery(ids []int64, timetables []model.Timetable) (string, []interface{}, error) {
	query := qbT.
		Insert(tableT).
		Columns(fieldsT).
		Suffix("ON CONFLICT (point_id) DO NOTHING")

	for i, timetable := range timetables {
		query = query.Values(ids[i],
			timetable.IsRoundTheClock,
			timetable.IsBreak,
			timetable.IsCampaign,
			timetable.IsFloating,
			timetable.WeekdaysJson[0],
			timetable.WeekdaysJson[1],
			timetable.WeekdaysJson[2],
			timetable.WeekdaysJson[3],
			timetable.WeekdaysJson[4],
			timetable.WeekdaysJson[5],
			timetable.WeekdaysJson[6],
			timetable.BreakStartTime.Time, timetable.BreakEndTime.Time,
			timetable.CampaignStartTime.Time, timetable.CampaignEndTime.Time)
	}

	return query.ToSql()
}

func formatUpdateTimetablesQuery(pointIds []int64, timetable *model.Timetable) (string, []interface{}, error) {
	if err := timetable.WeekdaysToJson(); err != nil {
		return "", nil, err
	}

	query := qbT.
		Update(tableT).
		Set("is_round_the_clock", timetable.IsRoundTheClock).
		Set("is_break", timetable.IsBreak).
		Set("is_campaign", timetable.IsCampaign).
		Set("is_floating", timetable.IsFloating).
		Set("monday", timetable.WeekdaysJson[0]).
		Set("tuesday", timetable.WeekdaysJson[1]).
		Set("wednesday", timetable.WeekdaysJson[2]).
		Set("thursday", timetable.WeekdaysJson[3]).
		Set("friday", timetable.WeekdaysJson[4]).
		Set("saturday", timetable.WeekdaysJson[5]).
		Set("sunday", timetable.WeekdaysJson[6]).
		Set("break_start_time", timetable.BreakStartTime.Time).
		Set("break_end_time", timetable.BreakEndTime.Time).
		Set("action_start_time", timetable.CampaignStartTime.Time).
		Set("action_end_time", timetable.CampaignEndTime.Time).
		Where(sq.Eq{"point_id": pointIds})

	return query.ToSql()
}

package point

import (
	"github.com/lib/pq"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

var columns = []string{"title", "description", "address", "city", "phones", "sites", "emails", "fractions"}

func pointToValues(point model.CollectionPoint) []interface{} {
	values := make([]interface{}, 0, 7)

	if point.Title == "" {
		values = append(values, nil)
	} else {
		values = append(values, point.Title)
	}

	if point.Description == "" {
		values = append(values, nil)
	} else {
		values = append(values, point.Description)
	}

	if point.Address == "" {
		values = append(values, nil)
	} else {
		values = append(values, point.Address)
	}

	if point.City == 0 {
		values = append(values, nil)
	} else {
		values = append(values, point.City)
	}

	if len(point.Phones) == 0 {
		values = append(values, nil)
	} else {
		values = append(values, pq.Array(point.Phones))
	}

	if len(point.Sites) == 0 {
		values = append(values, nil)
	} else {
		values = append(values, pq.Array(point.Sites))
	}

	if len(point.Emails) == 0 {
		values = append(values, nil)
	} else {
		values = append(values, pq.Array(point.Emails))
	}

	if len(point.RubbishType) == 0 {
		values = append(values, nil)
	} else {
		values = append(values, pq.Array(point.RubbishType))
	}

	return values
}

package point

import (
	sq "github.com/Masterminds/squirrel"
)

var qbF = sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

const (
	tableF  = `favorites`
	fieldsF = `"user_id", "point_id"`
)

func formatCreateFavouriteQuery(userId int64, ids []int64) (string, []interface{}, error) {
	query := qbT.
		Insert(tableF).
		Columns(fieldsF)

	for _, pointId := range ids {
		query = query.Values(userId, pointId)
	}

	return query.ToSql()
}

func formatDeleteFavouriteQuery(userId int64, ids []int64) (string, []interface{}, error) {
	return qbT.
		Delete(tableF).
		Where(sq.And{sq.Eq{"point_id": ids}, sq.Eq{"user_id": userId}}).
		ToSql()
}

func formatListFavoritesQuery(userId int64) (string, []interface{}, error) {
	return qb.
		Select(scanFields).
		From(table).
		Join("favorites on favorites.point_id = points.id").
		Join("timetables on timetables.point_id = points.id").
		Where(sq.Eq{"user_id": userId}).
		ToSql()
}

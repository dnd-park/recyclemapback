package point

import (
	"encoding/json"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v4"
	"github.com/lib/pq"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

var qb = sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

const (
	table  = `points`
	fields = `"title",
     "description",
     "address",
     "city",
     "images",
     "fractions",
     "phones",
     "sites",
     "emails",
     "tags",
     "created_by",
	 "updated_by",
	 "status"`
	limitedFields   = `"id", "title", ST_AsGeoJson("coordinates"), "fractions", "city"`
	insertFields    = fields + `, "coordinates"`
	scanFields      = `points."id",` + fields + `, grade, ST_AsGeoJson("coordinates"), updated_at, ` + fieldsWithoutPointId
	returningFields = ` RETURNING "id"`
)

func scanIds(rows pgx.Rows) ([]int64, error) {
	ids := make([]int64, 0)
	for rows.Next() {
		var id int64
		if err := rows.Scan(&id); err != nil {
			return nil, err
		}

		ids = append(ids, id)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return ids, nil
}

func scanUpdated(rows pgx.Rows) ([]model.CollectionPoint, error) {
	points := make([]model.CollectionPoint, 0)
	for rows.Next() {
		var point model.CollectionPoint
		if err := rows.Scan(&point.ID, &point.Title, &point.City); err != nil {
			return nil, err
		}

		points = append(points, point)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return points, nil
}

func scanLimitedPoints(rows pgx.Rows) ([]model.CollectionPoint, error) {
	points := make([]model.CollectionPoint, 0)
	for rows.Next() {
		var point model.CollectionPoint
		var fractions pq.Int64Array
		var coordinates string
		if err := rows.Scan(&point.ID, &point.Title, &coordinates, &fractions, &point.City); err != nil {
			return nil, err
		}

		err := json.Unmarshal([]byte(coordinates), &point.Coordinates)
		if err != nil {
			return nil, err
		}

		point.RubbishType = fractions
		points = append(points, point)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return points, nil
}

func scanPoints(rows pgx.Rows) ([]model.CollectionPoint, error) {
	points := make([]model.CollectionPoint, 0)
	for rows.Next() {
		var point model.CollectionPoint

		var images pq.StringArray
		var fractions pq.Int64Array
		var phones pq.StringArray
		var sites pq.StringArray
		var emails pq.StringArray
		var tags pq.StringArray
		var timetable model.Timetable

		var weekdays [7]string

		var coordinates string
		if err := rows.Scan(&point.ID, &point.Title, &point.Description,
			&point.Address, &point.City, &images, &fractions, &phones, &sites, &emails, &tags,
			&point.CreatedBy, &point.UpdatedBy, &point.Status, &point.AvgGrade, &coordinates, &point.UpdatedAt,
			&timetable.IsRoundTheClock, &timetable.IsBreak, &timetable.IsCampaign, &timetable.IsFloating,
			&weekdays[0], &weekdays[1], &weekdays[2], &weekdays[3], &weekdays[4], &weekdays[5], &weekdays[6],
			&timetable.BreakStartTime.Time, &timetable.BreakEndTime.Time,
			&timetable.CampaignStartTime.Time, &timetable.CampaignEndTime.Time,
		); err != nil {
			return nil, err
		}

		err := json.Unmarshal([]byte(coordinates), &point.Coordinates)
		if err != nil {
			return nil, err
		}

		point.Images = images
		point.RubbishType = fractions
		point.Phones = phones
		point.Sites = sites
		point.Emails = emails

		timetable.WeekdaysJson = weekdays[:]

		point.Timetable = &timetable

		_ = point.Timetable.WeekdaysFromJson()

		points = append(points, point)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return points, nil
}

func formatCreateQuery(points []model.CollectionPoint) (string, []interface{}, error) {
	query := qb.
		Insert(table).
		Columns(insertFields).
		Suffix(`ON CONFLICT (coordinates) DO UPDATE set title=EXCLUDED.title`).
		Suffix(returningFields)

	// TODO: скорее всего стоит поменять формат представления координат, это промежуточный варик
	// это не совсем так как просил Нозим, но как просил не работало
	for _, point := range points {
		point.Status = model.StatusPublished
		query = query.Values(point.Title, point.Description, point.Address, point.City,
			pq.Array(point.Images), pq.Array(point.RubbishType), pq.Array(point.Phones), pq.Array(point.Sites),
			pq.Array(point.Emails), pq.Array(point.Tags), point.CreatedBy, point.CreatedBy, point.Status,
			sq.Expr("ST_SetSRID(ST_MakePoint(?,?), 4326)", point.Longitude, point.Latitude)) // тот кто создал тот и первый обновил
	}

	return query.ToSql()
}

func formatGetQuery(ids []int64) (string, []interface{}, error) {
	return qb.
		Select(scanFields).
		From(table).
		Join("timetables on timetables.point_id = points.id").
		Where(sq.Eq{"points.id": ids}).
		ToSql()
}

func formatListNearestQuery(lat float64, long float64, limit uint64) (string, []interface{}, error) {
	orderByStr := fmt.Sprintf("coordinates <-> ST_SetSRID(ST_MakePoint(%f,%f), 4326)", long, lat)
	return qb.
		Select(limitedFields).
		From(table).
		OrderBy(orderByStr).
		Limit(limit).
		ToSql()
}

func formatListQuery(fractions []int64) (string, []interface{}, error) {
	return qb.
		Select(limitedFields).
		From(table).
		Where(sq.Eq{"status": model.StatusPublished}).
		Where("? = 0 OR fractions && ?", len(fractions), fractions).
		ToSql()
}

//Todo предлагаю удалить
func formatListFullQuery(limit uint64, page uint64) (string, []interface{}, error) {
	return qb.
		Select(scanFields).
		From(table).
		Where(sq.Eq{"status": model.StatusPublished}).
		Join("timetables on timetables.point_id = points.id").
		OrderBy("created_at desc").
		Offset(limit * (page - 1)).
		Limit(limit).
		ToSql()
}

func formatListFullQueryAdmin(params model.PointsParams) (string, []interface{}, error) {
	query := qb.
		Select(scanFields).
		From(table)

	if params.IsFilter {
		query = query.Where("fractions && ?", params.Filters)
	}

	return query.
		Join("timetables on timetables.point_id = points.id").
		OrderBy("created_at desc").
		Offset(params.Limit * (params.Page - 1)).
		Limit(params.Limit).
		ToSql()
}

func formatUpdateImages(id int64, images []string) (string, []interface{}, error) {
	return qb.Update(table).
		Set("images", sq.Expr("images || ?", pq.Array(images))).
		Where(sq.Eq{"id": id}).
		Suffix("returning id, title, city").
		ToSql()
}

func formatUpdateQuery(opts UpdateOpts) (string, []interface{}, error) {
	q := qb.Update(table)

	for i, name := range columns {
		if opts.Values != nil && len(opts.Values) > i && opts.Values[i] != nil {
			q = q.Set(name, opts.Values[i])
		}
	}

	q = q.Set("updated_by", opts.UpdatedBy).Where(sq.Eq{"id": opts.IDs})
	q = q.Suffix("returning id, title, city")

	return q.ToSql()
}

func formatUpdateStatusQuery(opts UpdateStatusOpts) (string, []interface{}, error) {
	return qb.Update(table).
		Set("status", opts.Status).
		Set("updated_by", opts.UpdatedBy).
		Where(sq.Eq{"id": opts.IDs}).
		Suffix("returning id, title, city").
		ToSql()
}

package point

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

const (
	LimitNearest = 15
)

type UpdateStatusOpts struct {
	IDs       []int64
	Status    string
	UpdatedBy int64
}

type UpdateOpts struct {
	IDs       []int64
	Values    []interface{}
	UpdatedBy int64
}

type Repository struct {
	db *pgxpool.Pool
}

func NewPointRepository(db *pgxpool.Pool) *Repository {
	return &Repository{db}
}

func (r *Repository) tx(ctx context.Context, fn func(pgx.Tx) error) error {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return fmt.Errorf("[pg] failed to begin transaction: %w", err)
	}

	err = fn(tx)
	if err != nil {
		rErr := tx.Rollback(ctx)
		if rErr != nil {
			return fmt.Errorf("[pg] failed to rollback transaction: %w", rErr)
		}
		return err
	}
	err = tx.Commit(ctx)
	if err != nil {
		return fmt.Errorf("[pg] failed to commit transaction: %w", err)
	}
	return nil
}

func (r *Repository) Create(points []model.CollectionPoint, timetables []model.Timetable) ([]int64, error) {
	var ids []int64
	if err := r.tx(context.Background(), func(tx pgx.Tx) error {
		q, args, err := formatCreateQuery(points)
		if err != nil {
			return err
		}

		rows, err := r.db.Query(context.Background(), q, args...)
		if err != nil {
			return err
		}
		defer rows.Close()

		ids, err = scanIds(rows)
		if err != nil {
			return err
		}

		q, args, err = formatCreateTimetablesQuery(ids, timetables)
		if err != nil {
			return err
		}

		_, err = r.db.Exec(context.Background(), q, args...)
		if err != nil {
			return err
		}
		return nil
	}); err != nil {
		return nil, err
	}

	return ids, nil
}

func (r *Repository) Get(ids []int64) ([]model.CollectionPoint, error) {
	q, args, err := formatGetQuery(ids)
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	points, err := scanPoints(rows)
	if err != nil {
		return nil, err
	}

	return points, nil
}

//TODO: протестировать на большем количестве фракций (посмотрела достаточно поверхостно)
// TODO: добавить блоки (поресечить как это делается в postgis)
func (r *Repository) List(fractions []int64) ([]model.CollectionPoint, error) {
	q, args, err := formatListQuery(fractions)
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	points, err := scanLimitedPoints(rows)
	if err != nil {
		return nil, err
	}

	return points, nil
}

func (r *Repository) ListFull(limit uint64, page uint64) ([]model.CollectionPoint, error) {
	q, args, err := formatListFullQuery(limit, page)
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	points, err := scanPoints(rows)
	if err != nil {
		return nil, err
	}

	return points, nil
}

func (r *Repository) ListFullAdmin(params model.PointsParams) ([]model.CollectionPoint, error) {
	q, args, err := formatListFullQueryAdmin(params)
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	points, err := scanPoints(rows)
	if err != nil {
		return nil, err
	}

	return points, nil
}

func (r *Repository) ListNearest(geo model.GeoPoint) ([]model.CollectionPoint, error) {
	q, args, err := formatListNearestQuery(geo.Latitude, geo.Longitude, LimitNearest)
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	points, err := scanLimitedPoints(rows)
	if err != nil {
		return nil, err
	}

	return points, nil
}

func (r *Repository) UpdateImages(id int64, images []string) ([]model.CollectionPoint, error) {
	q, args, err := formatUpdateImages(id, images)
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}

	points, err := scanUpdated(rows)
	if err != nil {
		return nil, err
	}

	return points, nil
}

func (r *Repository) Update(ids []int64, point *model.CollectionPoint) ([]model.CollectionPoint, error) {
	opts := UpdateOpts{
		IDs:       ids,
		Values:    pointToValues(*point),
		UpdatedBy: point.UpdatedBy,
	}

	var points []model.CollectionPoint
	if err := r.tx(context.Background(), func(tx pgx.Tx) error {
		q, args, err := formatUpdateQuery(opts)
		if err != nil {
			return err
		}

		rows, err := r.db.Query(context.Background(), q, args...)
		if err != nil {
			return err
		}

		points, err = scanUpdated(rows)
		if err != nil {
			return err
		}

		if point.Timetable == nil {
			return nil
		}

		q, args, err = formatUpdateTimetablesQuery(ids, point.Timetable)
		if err != nil {
			return err
		}

		_, err = r.db.Exec(context.Background(), q, args...)
		if err != nil {
			return err
		}
		return nil
	}); err != nil {
		return nil, err
	}

	return points, nil
}

func (r *Repository) UpdateStatus(ids []int64, point model.CollectionPoint) ([]model.CollectionPoint, error) {
	opts := UpdateStatusOpts{
		IDs:       ids,
		Status:    point.Status,
		UpdatedBy: point.UpdatedBy,
	}

	q, args, err := formatUpdateStatusQuery(opts)
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}

	points, err := scanUpdated(rows)
	if err != nil {
		return nil, err
	}

	return points, nil
}

func (r *Repository) CreateFavourites(userId int64, ids []int64) error {
	q, args, err := formatCreateFavouriteQuery(userId, ids)
	if err != nil {
		return err
	}

	_, err = r.db.Exec(context.Background(), q, args...)
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) DeleteFavourites(userId int64, ids []int64) error {
	q, args, err := formatDeleteFavouriteQuery(userId, ids)
	if err != nil {
		return err
	}

	_, err = r.db.Exec(context.Background(), q, args...)
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) ListFavorites(userId int64) ([]model.CollectionPoint, error) {
	q, args, err := formatListFavoritesQuery(userId)
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	points, err := scanPoints(rows)
	if err != nil {
		return nil, err
	}

	return points, nil
}

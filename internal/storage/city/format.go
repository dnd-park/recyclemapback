package city

import (
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v4"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

var qb = sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

const (
	table           = `"cities"`
	fields          = `"id", "name"`
	returningFields = ` RETURNING "id"`
)

func scanIds(rows pgx.Rows) ([]int64, error) {
	ids := make([]int64, 0)
	for rows.Next() {
		var id int64
		if err := rows.Scan(&id); err != nil {
			return nil, err
		}

		ids = append(ids, id)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return ids, nil
}

func scanCities(rows pgx.Rows) ([]model.City, error) {
	cities := make([]model.City, 0)
	for rows.Next() {
		var city model.City
		if err := rows.Scan(&city.ID, &city.Name); err != nil {
			return nil, err
		}

		cities = append(cities, city)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return cities, nil
}

func formatCreateQuery(cities []model.City) (string, []interface{}, error) {
	query := qb.
		Insert(table).
		Columns("name").
		Suffix(returningFields)

	for _, city := range cities {
		query = query.Values(city.Name)
	}

	return query.ToSql()
}

func formatListQuery() (string, []interface{}, error) {
	return qb.
		Select(fields).
		From(table).
		ToSql()
}

func formatGetQuery(ids []int64) (string, []interface{}, error) {
	return qb.
		Select(fields).
		From(table).
		Where(sq.Eq{"id": ids}).
		ToSql()
}

package city

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

type Repository struct {
	db *pgxpool.Pool
}

func NewCityRepository(db *pgxpool.Pool) *Repository {
	return &Repository{db}
}

func (r *Repository) tx(ctx context.Context, fn func(pgx.Tx) error) error {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return fmt.Errorf("[pg] failed to begin transaction: %w", err)
	}

	err = fn(tx)
	if err != nil {
		rErr := tx.Rollback(ctx)
		if rErr != nil {
			return fmt.Errorf("[pg] failed to rollback transaction: %w", rErr)
		}
		return err
	}
	err = tx.Commit(ctx)
	if err != nil {
		return fmt.Errorf("[pg] failed to commit transaction: %w", err)
	}
	return nil
}

func (r *Repository) Create(cities []model.City) ([]int64, error) {
	var ids []int64

	q, args, err := formatCreateQuery(cities)
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	ids, err = scanIds(rows)
	if err != nil {
		return nil, err
	}

	return ids, nil
}

func (r *Repository) Get(ids []int64) ([]model.City, error) {
	q, args, err := formatGetQuery(ids)
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	cities, err := scanCities(rows)
	if err != nil {
		return nil, err
	}

	return cities, nil
}

func (r *Repository) List() ([]model.City, error) {
	q, args, err := formatListQuery()
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	cities, err := scanCities(rows)
	if err != nil {
		return nil, err
	}

	return cities, nil
}

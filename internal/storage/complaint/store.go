package complaint

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

type Repository struct {
	db *pgxpool.Pool
}

func NewComplaintRepository(db *pgxpool.Pool) *Repository {
	return &Repository{db}
}

func (r *Repository) Create(compl *model.Complaint) error {
	q, args, err := formatCreateQuery(compl)
	if err != nil {
		return err
	}

	return r.db.QueryRow(context.Background(), q, args...).
		Scan(&compl.ID)
}

func (r *Repository) List(params model.ComplaintParams) ([]model.Complaint, error) {
	q, args, err := formatListQuery(params)
	if err != nil {
		return nil, err
	}
	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	return scanComplaints(rows)
}

func (r *Repository) Answer(compID int64, response string, responder int64) error {
	q, args, err := formatAnswerQuery(compID, response, responder)
	if err != nil {
		return err
	}
	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return err
	}

	defer rows.Close()

	return nil
}

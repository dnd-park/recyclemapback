package complaint

import (
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v4"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"time"
)

var qb = sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

const (
	table  = `complaints`
	fields = `"author_id", 
	 "point_id",
     "description",
     "email",
     "type"`
	scanFields      = `"id",` + fields + ` , "created"`
	limitedFields   = scanFields + `, "responder_id", "response", "updated", "is_answered"`
	returningFields = ` RETURNING "id"`
)

func formatCreateQuery(compl *model.Complaint) (string, []interface{}, error) {
	var email interface{}
	if compl.Email != "" {
		email = compl.Email
	} else {
		email = nil
	}

	query := qb.
		Insert(table).
		Columns(fields).
		Suffix(returningFields).
		Values(compl.Author, compl.PointID, compl.Description, email, compl.Type)

	return query.ToSql()
}

func formatListQuery(params model.ComplaintParams) (string, []interface{}, error) {
	query := qb.
		Select(limitedFields).
		From(table)

	if params.Answered {
		query = query.Where(sq.Eq{"is_answered": true})
	}

	if params.Email != "" {
		query = query.Where(sq.Eq{"email": params.Email})
	}

	if params.Author != 0 {
		query = query.Where(sq.Eq{"author_id": params.Author})
	}

	if params.ID != 0 {
		query = query.Where(sq.Eq{"id": params.ID})
	}

	if params.PointID != 0 {
		query = query.Where(sq.Eq{"point_id": params.PointID})
	}

	if params.ResponderID != 0 {
		query = query.Where(sq.Eq{"responder_id": params.ResponderID})
	}

	return query.
		OrderBy("created desc").
		Offset(params.Limit * (params.Page - 1)).
		Limit(params.Limit).
		ToSql()
}

func scanComplaints(rows pgx.Rows) ([]model.Complaint, error) {
	var email interface{}
	compls := make([]model.Complaint, 0)
	for rows.Next() {
		var compl model.Complaint

		if err := rows.Scan(&compl.ID, &compl.Author, &compl.PointID, &compl.Description,
			&email, &compl.Type, &compl.Created, &compl.ResponderID, &compl.Response,
			&compl.LastUpdated, &compl.IsAnswered); err != nil {
			return nil, err
		}

		if email != nil {
			compl.Email = email.(string)
		}

		compls = append(compls, compl)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return compls, nil
}

func formatAnswerQuery(compID int64, response string, responder int64) (string, []interface{}, error) {
	return qb.Update(table).
		Set("response", response).
		Set("responder_id", responder).
		Set("is_answered", true).
		Set("updated", time.Now()).
		Where(sq.Eq{"id": compID}).
		ToSql()
}

package comment

import (
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v4"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"time"
)

var qb = sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

const (
	table        = `comments`
	fieldsUpdate = `"point_id",
     "created_by",
     "comment",
     "created_at",
     "updated_at",
     "status"`
	fields = `"comments"."point_id",
     "comments"."created_by",
     "comments"."comment",
     "comments"."created_at",
     "comments"."updated_at",
     "comments"."status"`
	scanFields      = `"comments"."id",` + fields
	returningFields = ` RETURNING "id"`
)

func formatCreateQuery(cmnt *model.Comment) (string, []interface{}, error) {
	query := qb.
		Insert(table).
		Columns(fieldsUpdate).
		Suffix(returningFields).
		Values(cmnt.PointID, cmnt.Author.ID, cmnt.Comment, time.Now(), time.Now(), model.StatusPublished)

	return query.ToSql()
}

func formatDeleteQuery(id int64) (string, []interface{}, error) {
	query := qb.
		Update(table).
		Set("status", model.StatusDeleted).
		Set("updated_at", time.Now()).
		Where(sq.Eq{"id": id})

	return query.ToSql()
}

func formatListQuery(params model.CommentParams) (string, []interface{}, error) {
	query := qb.
		Select(scanFields + ", first_name, second_name").
		Join("users on users.id = comments.created_by").
		From(table)

	if params.ID != 0 {
		query = query.Where(sq.Eq{"comments.id": params.ID})
	}

	if params.PointID != 0 {
		query = query.Where(sq.Eq{"comments.point_id": params.PointID})
	}

	if params.Author != 0 {
		query = query.Where(sq.Eq{"comments.created_by": params.Author})
	}

	if params.Status != "" {
		query = query.Where(sq.Eq{"comments.status": params.Status})
	}

	return query.
		OrderBy("comments.created_at desc").
		Offset(params.Limit * (params.Page - 1)).
		Limit(params.Limit).
		ToSql()
}

func scanComments(rows pgx.Rows) ([]model.Comment, error) {
	cmnts := make([]model.Comment, 0)
	for rows.Next() {
		var cmnt model.Comment

		if err := rows.Scan(&cmnt.ID, &cmnt.PointID, &cmnt.Author.ID,
			&cmnt.Comment, &cmnt.Created, &cmnt.LastUpdated, &cmnt.Status,
			&cmnt.Author.FirstName, &cmnt.Author.SecondName); err != nil {
			return nil, err
		}

		cmnts = append(cmnts, cmnt)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return cmnts, nil
}

package comment

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

type Repository struct {
	db *pgxpool.Pool
}

func NewCommentRepository(db *pgxpool.Pool) *Repository {
	return &Repository{db}
}

func (r *Repository) Create(cmnt *model.Comment) error {
	q, args, err := formatCreateQuery(cmnt)
	if err != nil {
		return err
	}

	return r.db.QueryRow(context.Background(), q, args...).
		Scan(&cmnt.ID)
}

func (r *Repository) Delete(id int64) error {
	q, args, err := formatDeleteQuery(id)
	if err != nil {
		return err
	}

	_, err = r.db.Exec(context.Background(), q, args...)

	return err
}

func (r *Repository) List(params model.CommentParams) ([]model.Comment, error) {
	q, args, err := formatListQuery(params)
	if err != nil {
		return nil, err
	}
	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	return scanComments(rows)
}

package request_repo

import (
	"encoding/json"
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v4"
	"github.com/lib/pq"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

var qb = sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

const (
	table  = `requests`
	fields = `"title",
     "description",
     "address",
     "city",
     "images",
     "fractions",
     "phones",
     "sites",
     "emails",
     "created_by",
	 "updated_by",
     "point_id"`
	//limitedFields = `"id", "title", ST_AsGeoJson("coordinates"), "fractions"`
	scanFields      = `"id",` + fields + ` , "status", ST_AsGeoJson("coordinates"), "time"`
	insertFields    = fields + `, "status", "coordinates", "time"`
	returningFields = ` RETURNING "id"`
)

func formatCreateQuery(req *model.CollectionPoint) (string, []interface{}, error) {
	var point interface{}
	if req.PointID > 0 {
		point = req.PointID
	} else {
		point = nil
	}

	query := qb.
		Insert(table).
		Columns(insertFields).
		Suffix(returningFields).
		Values(req.Title, req.Description, req.Address, req.City,
			pq.Array(req.Images), pq.Array(req.RubbishType), pq.Array(req.Phones), pq.Array(req.Sites),
			pq.Array(req.Emails), req.CreatedBy, req.CreatedBy, point, req.Status,
			sq.Expr("ST_SetSRID(ST_MakePoint(?,?), 4326)", req.Longitude, req.Latitude), req.Time)

	return query.ToSql()
}

func formatListQuery(req model.FiltersReq) (string, []interface{}, error) {
	query := qb.
		Select(scanFields).
		From(table)

	if req.CreatorID > 0 {
		query = query.Where(sq.Eq{"created_by": req.CreatorID})
	}

	if req.Status != "" {
		query = query.Where(sq.Eq{"status": req.Status})
	}

	if req.City != 0 {
		query = query.Where(sq.Eq{"city": req.City})
	}

	if req.ID > 0 {
		query = query.Where(sq.Eq{"id": req.ID})
	}

	if req.IsFilter {
		query = query.Where("fractions && ?", req.Filters)
	}

	return query.
		OrderBy("created_at desc").
		Offset(req.Limit * (req.Page - 1)).
		Limit(req.Limit).
		ToSql()
}

func scanReqs(rows pgx.Rows) ([]model.CollectionPoint, error) {
	reqs := make([]model.CollectionPoint, 0)
	for rows.Next() {
		var point model.CollectionPoint

		var images pq.StringArray
		var fractions pq.Int64Array
		var phones pq.StringArray
		var sites pq.StringArray
		var emails pq.StringArray
		var PointID *int64

		var coordinates string
		if err := rows.Scan(&point.ID, &point.Title, &point.Description,
			&point.Address, &point.City, &images, &fractions, &phones, &sites, &emails,
			&point.CreatedBy, &point.UpdatedBy, &PointID, &point.Status, &coordinates,
			&point.Time); err != nil {
			return nil, err
		}

		if PointID != nil {
			point.PointID = *PointID
		}

		err := json.Unmarshal([]byte(coordinates), &point.Coordinates)
		if err != nil {
			return nil, err
		}

		point.Images = images
		point.RubbishType = fractions
		point.Phones = phones
		point.Sites = sites
		point.Emails = emails

		reqs = append(reqs, point)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return reqs, nil
}

func formatUpdateQuery(point model.CollectionPoint) (string, []interface{}, error) {
	return qb.Update(table).
		Set("title", point.Title).
		Set("description", point.Description).
		Set("address", point.Address).
		Set("city", point.City).
		Set("phones", pq.Array(point.Phones)).
		Set("sites", pq.Array(point.Sites)).
		Set("emails", pq.Array(point.Emails)).
		Set("status", point.Status).
		Set("fractions", point.RubbishType).
		Set("coordinates", sq.Expr("ST_SetSRID(ST_MakePoint(?,?), 4326)", point.Latitude, point.Longitude)).
		Set("updated_by", point.UpdatedBy).
		Set("time", point.Time).
		Where(sq.Eq{"id": point.ID}).
		ToSql()
}

func formatUpdateStatusQuery(status string, id int64, updatedID int64) (string, []interface{}, error) {
	return qb.Update(table).
		Set("status", status).
		Set("updated_by", updatedID).
		Where(sq.Eq{"id": id}).
		ToSql()
}

func formatUpdateImages(id int64, images []string) (string, []interface{}, error) {
	return qb.Update(table).
		Set("images", sq.Expr("images || ?", pq.Array(images))).
		Where(sq.Eq{"id": id}).
		ToSql()
}

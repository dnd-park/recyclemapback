package request_repo

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

type Repository struct {
	db *pgxpool.Pool
}

func NewRequestRepository(db *pgxpool.Pool) *Repository {
	return &Repository{db}
}

func (r *Repository) Create(req *model.CollectionPoint) (int64, error) {
	q, args, err := formatCreateQuery(req)
	if err != nil {
		return -1, err
	}

	err = r.db.QueryRow(context.Background(), q, args...).
		Scan(&req.ID)

	if err != nil {
		return -1, err
	}

	return req.ID, nil
}

func (r *Repository) List(reqFilters model.FiltersReq) ([]model.CollectionPoint, error) {
	q, args, err := formatListQuery(reqFilters)
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	return scanReqs(rows)
}

func (r *Repository) Update(req *model.CollectionPoint) error {
	q, args, err := formatUpdateQuery(*req)
	if err != nil {
		return err
	}

	_, err = r.db.Exec(context.Background(), q, args...)
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) SetStatus(status string, id int64, updatedBy int64) error {
	q, args, err := formatUpdateStatusQuery(status, id, updatedBy)
	if err != nil {
		return err
	}

	_, err = r.db.Exec(context.Background(), q, args...)
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) Get(filters model.FiltersReq) (*model.CollectionPoint, error) {
	q, args, err := formatListQuery(filters)
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	reqs, err := scanReqs(rows)
	if err != nil {
		return nil, err
	}
	return &reqs[0], nil
}

func (r *Repository) UpdateImages(id int64, images []string) error {
	q, args, err := formatUpdateImages(id, images)
	if err != nil {
		return err
	}

	_, err = r.db.Exec(context.Background(), q, args...)
	if err != nil {
		return err
	}

	return nil
}

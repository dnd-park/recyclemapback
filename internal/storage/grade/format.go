package grade

import (
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v4"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

var qb = sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

const (
	table  = `grades`
	fields = `"user_id", "point_id", "grade"`
)

func scanIds(rows pgx.Rows) ([]int64, error) {
	ids := make([]int64, 0)
	for rows.Next() {
		var id int64
		if err := rows.Scan(&id); err != nil {
			return nil, err
		}

		ids = append(ids, id)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return ids, nil
}

func formatCreateQuery(userID int64, grades []model.Grade) (string, []interface{}, error) {
	query := qb.
		Insert(table).
		Columns(fields).
		Suffix("ON CONFLICT (user_id, point_id) DO UPDATE SET grade=EXCLUDED.grade RETURNING point_id")

	for _, g := range grades {
		query = query.Values(userID, g.PointID, g.Grade)
	}

	return query.ToSql()
}

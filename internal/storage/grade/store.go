package grade

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

type Repository struct {
	db *pgxpool.Pool
}

func NewGradeRepository(db *pgxpool.Pool) *Repository {
	return &Repository{db}
}

func (r *Repository) Create(userID int64, grades []model.Grade) ([]int64, error) {
	q, args, err := formatCreateQuery(userID, grades)
	if err != nil {
		return nil, err
	}

	rows, err := r.db.Query(context.Background(), q, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	return scanIds(rows)
}

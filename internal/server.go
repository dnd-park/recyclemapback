package internal

import (
	"context"
	"database/sql"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/jackc/pgx/v4/pgxpool"
	actionH "gitlab.com/dnd-park/recyclemapback/internal/handlers/action"
	adminH "gitlab.com/dnd-park/recyclemapback/internal/handlers/admin"
	cityH "gitlab.com/dnd-park/recyclemapback/internal/handlers/city"
	commentH "gitlab.com/dnd-park/recyclemapback/internal/handlers/comment"
	favoriteH "gitlab.com/dnd-park/recyclemapback/internal/handlers/favorite"
	gradeH "gitlab.com/dnd-park/recyclemapback/internal/handlers/grade"
	pointH "gitlab.com/dnd-park/recyclemapback/internal/handlers/point"
	userH "gitlab.com/dnd-park/recyclemapback/internal/handlers/user"
	"gitlab.com/dnd-park/recyclemapback/internal/middleware"
	"gitlab.com/dnd-park/recyclemapback/internal/respond"
	"gitlab.com/dnd-park/recyclemapback/internal/storage/action"
	"gitlab.com/dnd-park/recyclemapback/internal/storage/city"
	"gitlab.com/dnd-park/recyclemapback/internal/storage/comment"
	"gitlab.com/dnd-park/recyclemapback/internal/storage/complaint"
	"gitlab.com/dnd-park/recyclemapback/internal/storage/grade"
	"gitlab.com/dnd-park/recyclemapback/internal/storage/point"
	request_repo "gitlab.com/dnd-park/recyclemapback/internal/storage/request/pg"
	"gitlab.com/dnd-park/recyclemapback/internal/storage/user"
	actionU "gitlab.com/dnd-park/recyclemapback/internal/usecases/action"
	adminU "gitlab.com/dnd-park/recyclemapback/internal/usecases/admin"
	cityU "gitlab.com/dnd-park/recyclemapback/internal/usecases/city"
	commentU "gitlab.com/dnd-park/recyclemapback/internal/usecases/comment"
	complaint2 "gitlab.com/dnd-park/recyclemapback/internal/usecases/complaint"
	favoriteU "gitlab.com/dnd-park/recyclemapback/internal/usecases/favorite"
	gradeU "gitlab.com/dnd-park/recyclemapback/internal/usecases/grade"
	pointU "gitlab.com/dnd-park/recyclemapback/internal/usecases/point"
	"gitlab.com/dnd-park/recyclemapback/internal/usecases/request"
	userU "gitlab.com/dnd-park/recyclemapback/internal/usecases/user"
	"go.uber.org/zap"
	"net/http"
)

type Server struct {
	Mux          *mux.Router
	Config       *Config
	Logger       *zap.SugaredLogger
	SessionStore sessions.Store
}

func NewServer(config *Config, logger *zap.SugaredLogger) (*Server, error) {
	s := &Server{
		Mux:          mux.NewRouter(),
		Logger:       logger,
		Config:       config,
		SessionStore: sessions.NewCookieStore([]byte(config.SessionKey)),
	}
	return s, nil
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.Mux.ServeHTTP(w, r)
}

func (s *Server) ConfigureServer(db *sql.DB) {
	conn, _ := pgxpool.Connect(context.Background(), s.Config.DatabaseURL)

	responder := respond.Responder{Logger: s.Logger}

	userRepo := user.NewUserRepository(conn)
	pointRepo := point.NewPointRepository(conn)
	reqRepo := request_repo.NewRequestRepository(conn)
	cityRepo := city.NewCityRepository(conn)
	complRepo := complaint.NewComplaintRepository(conn)
	gradeRepo := grade.NewGradeRepository(conn)
	commentRepo := comment.NewCommentRepository(conn)
	actionRepo := action.NewActionRepository(conn)

	actionUC := actionU.NewActionUsecase(actionRepo)
	pointUC := pointU.NewPointUsecase(pointRepo, actionUC)
	userUC := userU.NewUserUsecase(userRepo)
	adminUC := adminU.NewAdminUsecase(userRepo, pointRepo, reqRepo, actionUC)
	reqUC := request.NewRequestUsecase(reqRepo)
	cityUC := cityU.NewCityUsecase(cityRepo)
	favUC := favoriteU.NewFavoriteUsecase(pointRepo)
	complUC := complaint2.NewComplaintUsecase(complRepo)
	gradeUC := gradeU.NewGradeUsecase(gradeRepo)
	commentUC := commentU.NewCommentUsecase(commentRepo)

	mid := middleware.NewMiddleware(s.Logger, s.Config.ClientUrl, s.SessionStore, *userUC, &responder)
	private := s.Mux.PathPrefix("").Subrouter()
	adminMux := s.Mux.PathPrefix("/admin").Subrouter()

	s.Mux.Use(mid.CORSMiddleware)
	private.Use(mid.CORSMiddleware, mid.AuthenticateUser)
	adminMux.Use(mid.CORSMiddleware, mid.AdminMiddleware)

	userH.NewUserHandler(s.Mux, private, *userUC, *reqUC, *complUC, s.Logger, s.SessionStore, &responder)
	adminH.NewAdminHandler(adminMux, *adminUC, *userUC, *reqUC, *pointUC, *complUC, s.Logger, s.SessionStore, &responder)
	pointH.NewPointHandler(s.Mux, private, *pointUC, s.Logger, s.SessionStore, *complUC, &responder)
	cityH.NewCityHandler(s.Mux, cityUC, s.Logger, s.SessionStore, &responder)
	favoriteH.NewFavoriteHandler(private, favUC, s.Logger, s.SessionStore, &responder)
	gradeH.NewGradeHandler(private, s.Logger, s.SessionStore, &responder, gradeUC)
	commentH.NewCommentHandler(s.Mux, private, adminMux, *commentUC, *userUC, *pointUC, s.Logger, s.SessionStore, &responder)
	actionH.NewActionHandler(s.Mux, s.Logger, s.SessionStore, &responder, actionUC)

	return
}

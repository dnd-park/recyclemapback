package respond

import (
	"encoding/json"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"net/http"
)

type ctxKey int8

const (
	SessionName        = "user-session"
	CtxKeyUser  ctxKey = iota
)

type Responder struct {
	Logger *zap.SugaredLogger
}

func (resp *Responder) Error(w http.ResponseWriter, r *http.Request, code int, err error) {
	resp.logData(r.URL.Path, code, err)
	w.WriteHeader(code)
	data := map[string]string{"message": errors.Cause(err).Error()}
	_ = json.NewEncoder(w).Encode(data)
}

func (resp *Responder) Respond(w http.ResponseWriter, r *http.Request, code int, data interface{}) {
	w.WriteHeader(code)
	_ = json.NewEncoder(w).Encode(data)
	resp.logData(r.URL.Path, code, nil)
}

func (resp *Responder) logData(url string, status int, err error) {
	if err == nil {
		resp.Logger.Infof(` 'path' : '%v', 'CODE' : [%v] `, url, status)
	} else {
		resp.Logger.Errorf(` 'path' : '%v', 'CODE' : [%v], 'error' : '%v'`, url, status, err.Error())
	}
}

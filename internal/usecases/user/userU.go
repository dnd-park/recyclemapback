package user

import (
	"github.com/pkg/errors"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"log"
)

type repository interface {
	Get(field string, data interface{}) ([]model.User, error)
	Create(user *model.User) error
	Update(user *model.User) error
	UpdateLastLogin(id int64) error
	List(params *model.UserFilter) ([]model.User, error)
	CreateVkUser(us *model.VkUser) (int64, error)
	LinkVkToEmail(us *model.VkUser) (int64, error)
}

type UserUsecase struct {
	repo repository
}

func NewUserUsecase(repo repository) *UserUsecase {
	return &UserUsecase{
		repo: repo,
	}
}

func (u *UserUsecase) SignUp(user *model.User) error {
	err := user.BeforeCreate()
	if err != nil {
		return err
	}

	return u.repo.Create(user)
}

func (u *UserUsecase) VerifyUser(currUser *model.User) (*model.User, error) {
	users, err := u.repo.Get("email", currUser.Email)
	if err != nil {
		log.Println(err)
		return nil, errors.New("Аккаунта с данными email не существует")
	}

	if len(users) < 1 {
		log.Println(len(users))
		return nil, errors.New("Аккаунта с данными email не существует")
	}

	if !users[0].ComparePassword(currUser.Password) {
		return nil, errors.New("Неверный пароль")
	}

	return &users[0], nil
}

func (u *UserUsecase) Get(id int64) (*model.User, error) {
	users, err := u.repo.Get("id", id)
	if err != nil {
		return nil, err
	}

	if len(users) < 1 {
		return nil, errors.New("empty result")
	}

	return &users[0], nil
}

func (u *UserUsecase) EditUser(user *model.User) error {
	existUser, err := u.Get(user.ID)
	if err != nil {
		return errors.New("user not exist")
	}

	// TODO: уточнить у ребят по апи
	existUser.FirstName = user.FirstName
	existUser.SecondName = user.SecondName
	existUser.City = user.City
	existUser.Phones = user.Phones
	//existUser.Avatar = user.Avatar

	return u.repo.Update(user)
}

// TODO: еще может ошибку возвращать если не нашлось пользователя
func (u *UserUsecase) IsAdmin(id int64) bool {
	user, err := u.Get(id)
	if err != nil {
		return false
	}

	return user.UserType == "admin"
}

func (u *UserUsecase) UpdateLastLogin(id int64) error {
	return u.repo.UpdateLastLogin(id)
}

func (u *UserUsecase) List(params *model.UserFilter) ([]model.User, error) {
	users, err := u.repo.List(params)
	if err != nil {
		return nil, err
	}

	return users, nil
}

func (u *UserUsecase) AuthVk(vkUser *model.VkUser) ([]model.User, error) {
	var IsExistEmail bool
	var IsExistVk bool

	if vkUser.Email != "" {
		users, err := u.repo.Get("email", vkUser.Email)
		if err != nil {
			return nil, err
		}
		if len(users) > 0 {
			IsExistEmail = true
		}

	}

	users, err := u.repo.Get("vk_id", vkUser.VkID)
	if err != nil {
		return nil, err
	}

	if len(users) > 0 {
		IsExistVk = true
	}

	if !IsExistVk && !IsExistEmail {
		id, err := u.repo.CreateVkUser(vkUser)
		if err != nil {
			return nil, err
		}

		return u.repo.Get("id", id)
	}

	if !IsExistVk && IsExistEmail {
		id, err := u.repo.LinkVkToEmail(vkUser)
		if err != nil {
			return nil, err
		}

		return u.repo.Get("id", id)
	}

	return users, err
}

package favorite

import (
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"net/http"
)

type repository interface {
	CreateFavourites(userId int64, ids []int64) error
	DeleteFavourites(userId int64, ids []int64) error
	ListFavorites(userId int64) ([]model.CollectionPoint, error)
}

type FavoriteUsecase struct {
	repo repository
}

func NewFavoriteUsecase(repo repository) *FavoriteUsecase {
	return &FavoriteUsecase{
		repo: repo,
	}
}

func (u *FavoriteUsecase) Create(userId int64, ids []int64) error {
	if ids == nil || len(ids) == 0 {
		return nil
	}

	if err := u.repo.CreateFavourites(userId, ids); err != nil {
		return &model.HttpError{
			StatusCode: http.StatusInternalServerError,
			StringErr:  err.Error(),
		}
	}

	return nil
}

func (u *FavoriteUsecase) Delete(userId int64, ids []int64) error {
	if ids == nil || len(ids) == 0 {
		return nil
	}

	if err := u.repo.DeleteFavourites(userId, ids); err != nil {
		return &model.HttpError{
			StatusCode: http.StatusInternalServerError,
			StringErr:  err.Error(),
		}
	}

	return nil
}

func (u *FavoriteUsecase) List(userId int64) ([]model.CollectionPoint, error) {
	points, err := u.repo.ListFavorites(userId)
	if err != nil {
		return nil, &model.HttpError{
			StatusCode: http.StatusNotFound,
			StringErr:  err.Error(),
		}
	}
	return points, nil
}

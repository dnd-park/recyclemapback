package request

import (
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"net/http"
)

type repository interface {
	Create(req *model.CollectionPoint) (int64, error)
	List(filters model.FiltersReq) ([]model.CollectionPoint, error)
	UpdateImages(id int64, images []string) error
	Update(req *model.CollectionPoint) error
}

type RequestUsecase struct {
	repo repository
}

func NewRequestUsecase(repo repository) *RequestUsecase {
	return &RequestUsecase{
		repo: repo,
	}
}

func (u *RequestUsecase) CreateRequest(request *model.CollectionPoint) (int64, error) {
	request.Status = model.StatusOnCheck
	id, err := u.repo.Create(request)
	if err != nil {
		return 0, &model.HttpError{
			StatusCode: http.StatusBadRequest,
			StringErr:  err.Error(),
		}
	}

	return id, nil
}

func (u *RequestUsecase) ListRequests(filters model.FiltersReq) ([]model.CollectionPoint, error) {
	reqs, err := u.repo.List(filters)
	if err != nil {
		return nil, &model.HttpError{
			StatusCode: http.StatusBadRequest,
			StringErr:  err.Error(),
		}
	}

	return reqs, nil
}

func (u *RequestUsecase) Update(req *model.CollectionPoint) error {
	req.Status = model.StatusOnCheck
	return u.repo.Update(req)
}

func (u *RequestUsecase) AddImages(pointId int64, images []string) error {
	if images == nil || len(images) == 0 {
		return nil
	}

	if err := u.repo.UpdateImages(pointId, images); err != nil {
		return &model.HttpError{
			StatusCode: http.StatusInternalServerError,
			StringErr:  err.Error(),
		}
	}

	return nil
}

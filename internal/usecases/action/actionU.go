package action

import (
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"log"
	"net/http"
)

type repository interface {
	Create(actor *model.ActionItem, actions []model.Action) error
	List(params model.ActionParams) ([]model.Action, error)
}

type Usecase struct {
	repo repository
}

func NewActionUsecase(repository repository) *Usecase {
	return &Usecase{
		repo: repository,
	}
}

func (u *Usecase) create(actor *model.ActionItem, actions []model.Action) error {
	if actions == nil || len(actions) == 0 {
		return nil
	}
	return u.repo.Create(actor, actions)
}

func (u *Usecase) CreateActions(actor *model.ActionItem, items []model.ActionItem, actionTypes []string, cities []int64) {
	actions := make([]model.Action, 0, len(items))

	for i, _ := range items {
		action := model.Action{
			Action: actionTypes[i],
			Item:   items[i],
			City:   cities[i],
		}
		actions = append(actions, action)
	}

	if err := u.create(actor, actions); err != nil {
		log.Println(err)
	}
}

func (u *Usecase) List(params model.ActionParams) ([]model.Action, error) {
	actions, err := u.repo.List(params)
	if err != nil {
		return nil, &model.HttpError{
			StatusCode: http.StatusInternalServerError,
			StringErr:  err.Error(),
		}
	}
	return actions, nil
}

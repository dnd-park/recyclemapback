package grade

import (
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"net/http"
)

type repository interface {
	Create(userID int64, grades []model.Grade) ([]int64, error)
}

type Usecase struct {
	repo repository
}

func NewGradeUsecase(repo repository) *Usecase {
	return &Usecase{
		repo: repo,
	}
}

func (u *Usecase) Create(userID int64, grades []model.Grade) ([]int64, error) {
	if grades == nil {
		return nil, nil
	}

	ids, err := u.repo.Create(userID, grades)
	if err != nil {
		return nil, &model.HttpError{
			StatusCode: http.StatusInternalServerError,
			StringErr:  err.Error(),
		}
	}

	return ids, nil
}

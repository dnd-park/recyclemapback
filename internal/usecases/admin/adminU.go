package admin

import (
	"encoding/json"
	"errors"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"strings"
)

const (
	DefaultLimit = 50
	DefaultPage  = 1
)

type userRepository interface {
	// TODO: переделать на функцию UpdateStatus(id []int64) error в новом репозитории
	UpdateStatus(ids int64, newType string, cityID int64) error
}

type pointRepository interface {
	Create(points []model.CollectionPoint, timetables []model.Timetable) ([]int64, error)
	Update(ids []int64, point *model.CollectionPoint) ([]model.CollectionPoint, error)
}

type reqRepository interface {
	Create(req *model.CollectionPoint) (int64, error)
	//Get(ids []int64) ([]model.CollectionPoint, error)
	List(filters model.FiltersReq) ([]model.CollectionPoint, error)
	Update(req *model.CollectionPoint) error
	SetStatus(status string, id int64, updatedBy int64) error
	Get(filters model.FiltersReq) (*model.CollectionPoint, error)
}

type actionUcase interface {
	CreateActions(actor *model.ActionItem, items []model.ActionItem, actionTypes []string, cities []int64)
}

type AdminUsecase struct {
	userRep  userRepository
	pointRep pointRepository
	reqRep   reqRepository
	actionU  actionUcase
}

func NewAdminUsecase(u userRepository, p pointRepository, r reqRepository, a actionUcase) *AdminUsecase {
	return &AdminUsecase{
		userRep:  u,
		pointRep: p,
		reqRep:   r,
		actionU:  a,
	}
}

func (u *AdminUsecase) NewAdmin(sess *model.SessionData, itemID int64, newType string, cityID int64) error {
	item := model.ActionItem{
		ID:   itemID,
		Type: model.ActionUser,
	}

	actor := &model.ActionItem{
		ID:   sess.ID,
		Name: sess.Name,
	}

	actionType := model.ActionGrantAdmin
	if newType == model.ModeratorType {
		actionType = model.ActionGrantModerator
	}

	err := u.userRep.UpdateStatus(itemID, newType, cityID)
	if err != nil {
		return err
	}

	u.actionU.CreateActions(actor, []model.ActionItem{item}, []string{actionType}, []int64{cityID})

	return nil
}

func (u *AdminUsecase) ApproveRequest(sess *model.SessionData, id int64) error {
	var points []model.CollectionPoint
	var filters model.FiltersReq
	var timetable model.Timetable
	filters.ID = id
	filters.Limit = 1
	filters.Page = DefaultPage

	req, err := u.reqRep.Get(filters)
	if err != nil {
		return err
	}

	req.UpdatedBy = sess.ID
	req.Longitude = req.Coordinates.Coordinates[0]
	req.Latitude = req.Coordinates.Coordinates[1]
	req.Status = model.StatusPublished

	req.Time = strings.ReplaceAll(req.Time, `'`, `"`)
	reader := strings.NewReader(req.Time)

	decoder := json.NewDecoder(reader)
	err = decoder.Decode(&timetable)
	if err != nil {
		return errors.New("error unmarshaling timetable")
	}
	req.Timetable = &timetable

	err = u.reqRep.SetStatus(model.StatusApproved, id, sess.ID)
	if err != nil {
		return err
	}

	if req.PointID > 0 {
		_, err = u.pointRep.Update([]int64{req.PointID}, req)
		if err != nil {
			return errors.New("error updating point")
		}

		return nil
	}

	points = append(points, *req)

	timetables := make([]model.Timetable, 0, len(points))
	for _, point := range points {
		if err := point.Timetable.WeekdaysToJson(); err != nil {
			return err
		}
		timetables = append(timetables, *point.Timetable)
	}

	ids, err := u.pointRep.Create(points, timetables)
	if err != nil {
		return err
	}

	actionPoint := model.ActionItem{
		ID:   ids[0],
		Type: model.ActionPoint,
		Name: req.Title,
	}

	actionRequest := model.ActionItem{
		ID:   id,
		Type: model.ActionRequest,
		Name: req.Title,
	}

	u.actionU.CreateActions(
		&model.ActionItem{ID: sess.ID, Name: sess.Name},
		[]model.ActionItem{actionRequest, actionPoint},
		[]string{model.ActionApproved, model.ActionCreated},
		[]int64{int64(req.City), int64(req.City)})

	return nil
}

func (u *AdminUsecase) DenyRequest(sess *model.SessionData, id int64) error {
	if err := u.reqRep.SetStatus(model.StatusDenied, id, sess.ID); err != nil {
		return err
	}

	u.actionU.CreateActions(
		&model.ActionItem{ID: sess.ID, Name: sess.Name},
		[]model.ActionItem{{ID: id, Type: model.ActionRequest}},
		[]string{model.ActionDenied},
		[]int64{533},
	)

	return nil
}

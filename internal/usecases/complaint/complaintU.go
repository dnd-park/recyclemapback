package complaint

import (
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

type complaintRepository interface {
	Create(complaint *model.Complaint) error
	List(params model.ComplaintParams) ([]model.Complaint, error)
	Answer(compID int64, response string, responder int64) error
}

type ComplaintUsecase struct {
	repo complaintRepository
}

func NewComplaintUsecase(c complaintRepository) *ComplaintUsecase {
	return &ComplaintUsecase{
		repo: c,
	}
}

func (c *ComplaintUsecase) Create(complaint *model.Complaint) error {
	if complaint == nil {
		return nil
	}

	return c.repo.Create(complaint)
}

func (c *ComplaintUsecase) List(params model.ComplaintParams) ([]model.Complaint, error) {
	return c.repo.List(params)
}

func (c *ComplaintUsecase) Answer(compID int64, response string, responder int64) error {
	return c.repo.Answer(compID, response, responder)
}

package comment

import (
	"errors"
	"gitlab.com/dnd-park/recyclemapback/internal/handlers/helpers"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
)

type commentRepository interface {
	Create(cmnt *model.Comment) error
	List(params model.CommentParams) ([]model.Comment, error)
	Delete(id int64) error
	//Answer(compID int64, response string, responder int64) error
}

type CommentUsecase struct {
	repo commentRepository
}

func NewCommentUsecase(c commentRepository) *CommentUsecase {
	return &CommentUsecase{
		repo: c,
	}
}

func (c *CommentUsecase) Create(cmnt *model.Comment) error {
	if cmnt == nil {
		return nil
	}

	return c.repo.Create(cmnt)
}

func (c *CommentUsecase) Delete(id int64, author int64) error {
	params := model.CommentParams{
		ID:     id,
		Status: model.StatusPublished,
		Author: author,
		Page:   helpers.DefaultPage,
		Limit:  helpers.DefaultLimit,
	}

	comments, err := c.List(params)
	if err != nil {
		return err
	}

	if len(comments) == 0 {
		return errors.New("комментарий не найден")
	}

	return c.repo.Delete(id)
}

func (c *CommentUsecase) List(params model.CommentParams) ([]model.Comment, error) {
	return c.repo.List(params)
}

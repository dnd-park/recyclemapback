package point

import (
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"net/http"
)

type repository interface {
	Create(points []model.CollectionPoint, timetables []model.Timetable) ([]int64, error)
	Get(ids []int64) ([]model.CollectionPoint, error)
	List(fractions []int64) ([]model.CollectionPoint, error)
	ListNearest(geo model.GeoPoint) ([]model.CollectionPoint, error)
	ListFull(limit uint64, page uint64) ([]model.CollectionPoint, error) // TODO предлагаю удалить
	ListFullAdmin(params model.PointsParams) ([]model.CollectionPoint, error)
	Update(ids []int64, point *model.CollectionPoint) ([]model.CollectionPoint, error)
	UpdateImages(id int64, images []string) ([]model.CollectionPoint, error)
	UpdateStatus(ids []int64, point model.CollectionPoint) ([]model.CollectionPoint, error)
}

type actionUcase interface {
	CreateActions(actor *model.ActionItem, items []model.ActionItem, actionTypes []string, cities []int64)
}

type PointUsecase struct {
	repo    repository
	actionU actionUcase
}

func NewPointUsecase(repo repository, ucase actionUcase) *PointUsecase {
	return &PointUsecase{
		repo:    repo,
		actionU: ucase,
	}
}

func (u *PointUsecase) Get(id int64) (*model.CollectionPoint, error) {
	points, err := u.repo.Get([]int64{id})
	if err != nil {
		return nil, &model.HttpError{
			StatusCode: http.StatusInternalServerError,
			StringErr:  err.Error(),
		}
	}

	if len(points) < 1 {
		return nil, &model.HttpError{
			StatusCode: http.StatusNotFound,
			StringErr:  "empty result",
		}
	}

	return &points[0], nil
}

func (u *PointUsecase) CreatePoint(sess *model.SessionData, point *model.CollectionPoint) (int64, error) {
	if point == nil {
		return 0, nil
	}

	actor := &model.ActionItem{ID: sess.ID, Name: sess.Name}

	point.CreatedBy = actor.ID
	point.UpdatedBy = actor.ID

	if err := point.Timetable.WeekdaysToJson(); err != nil {
		return 0, &model.HttpError{
			StatusCode: http.StatusNotFound,
			StringErr:  "invalid data: timetable",
		}
	}

	timetables := []model.Timetable{*point.Timetable}
	points := []model.CollectionPoint{*point}

	ids, err := u.repo.Create(points, timetables)
	if err != nil {
		return 0, &model.HttpError{
			StatusCode: http.StatusInternalServerError,
			StringErr:  err.Error(),
		}
	}

	point.ID = ids[0]

	u.createPointsAction(actor, []model.CollectionPoint{*point}, model.ActionCreated)

	return ids[0], nil
}

func (u *PointUsecase) AddImages(pointId int64, images []string) error {
	if images == nil || len(images) == 0 {
		return nil
	}

	if _, err := u.repo.UpdateImages(pointId, images); err != nil {
		return &model.HttpError{
			StatusCode: http.StatusInternalServerError,
			StringErr:  err.Error(),
		}
	}

	return nil
}

func (u *PointUsecase) CreatePoints(sess *model.SessionData, points []model.CollectionPoint) ([]int64, error) {
	if len(points) == 0 {
		return nil, nil
	}

	actor := &model.ActionItem{
		ID:   sess.ID,
		Name: sess.Name,
	}

	timetables := make([]model.Timetable, 0, len(points))
	for i, point := range points {
		points[i].CreatedBy = actor.ID
		points[i].UpdatedBy = actor.ID

		if err := point.Timetable.WeekdaysToJson(); err != nil {
			return nil, &model.HttpError{
				StatusCode: http.StatusBadRequest,
				StringErr:  err.Error(),
			}
		}
		timetables = append(timetables, *point.Timetable)
	}

	ids, err := u.repo.Create(points, timetables)
	if err != nil {
		return nil, &model.HttpError{
			StatusCode: http.StatusInternalServerError,
			StringErr:  err.Error(),
		}
	}

	func() {
		items := make([]model.ActionItem, 0, len(ids))
		actions := make([]string, 0, len(ids))
		cities := make([]int64, 0, len(ids))

		for i, _ := range ids {
			item := model.ActionItem{
				ID:   ids[i],
				Type: model.ActionPoint,
				Name: points[i].Title,
			}
			items = append(items, item)
			actions = append(actions, model.ActionCreated)
			cities = append(cities, int64(points[i].City))
		}

		u.actionU.CreateActions(actor, items, actions, cities)
	}()

	return ids, nil
}

func (u *PointUsecase) List(params model.PointsParams) ([]model.CollectionPoint, error) {
	points, err := u.repo.List(params.Filters)
	if err != nil {
		return nil, &model.HttpError{
			StatusCode: http.StatusNotFound,
			StringErr:  err.Error(),
		}
	}

	return points, nil
}

func (u *PointUsecase) ListNearest(geo model.GeoPoint) ([]model.CollectionPoint, error) {
	points, err := u.repo.ListNearest(geo)
	if err != nil {
		return nil, &model.HttpError{
			StatusCode: http.StatusNotFound,
			StringErr:  err.Error(),
		}
	}

	return points, nil
}

// TODO предлагаю удалить
func (u *PointUsecase) ListFull(limit uint64, page uint64) ([]model.CollectionPoint, error) {
	points, err := u.repo.ListFull(limit, page)
	if err != nil {
		return nil, &model.HttpError{
			StatusCode: http.StatusNotFound,
			StringErr:  err.Error(),
		}
	}

	return points, nil
}

func (u *PointUsecase) ListFullAdmin(params model.PointsParams) ([]model.CollectionPoint, error) {
	points, err := u.repo.ListFullAdmin(params)
	if err != nil {
		return nil, &model.HttpError{
			StatusCode: http.StatusNotFound,
			StringErr:  err.Error(),
		}
	}

	return points, nil
}

func (u *PointUsecase) Update(sess *model.SessionData, ids []int64, point *model.CollectionPoint) error {
	actor := &model.ActionItem{
		ID:   sess.ID,
		Name: sess.Name,
	}

	point.UpdatedBy = actor.ID

	upPoints, err := u.repo.Update(ids, point)
	if err != nil {
		return &model.HttpError{
			StatusCode: http.StatusInternalServerError,
			StringErr:  err.Error(),
		}
	}

	u.createPointsAction(actor, upPoints, model.ActionUpdated)

	return nil
}

func (u *PointUsecase) createPointsAction(actor *model.ActionItem, upPoints []model.CollectionPoint, actionType string) {
	items := make([]model.ActionItem, 0, len(upPoints))
	actions := make([]string, 0, len(upPoints))
	cities := make([]int64, 0, len(upPoints))

	for _, point := range upPoints {
		item := model.ActionItem{
			ID:   point.ID,
			Name: point.Title,
			Type: model.ActionPoint,
		}
		items = append(items, item)
		actions = append(actions, actionType)
		cities = append(cities, int64(point.City))
	}

	u.actionU.CreateActions(actor, items, actions, cities)
}

func (u *PointUsecase) Publish(sess *model.SessionData, ids []int64) error {
	var point model.CollectionPoint

	actor := &model.ActionItem{
		ID:   sess.ID,
		Name: sess.Name,
	}

	point.Status = model.StatusPublished
	point.UpdatedBy = actor.ID

	upPoints, err := u.repo.UpdateStatus(ids, point)
	if err != nil {
		return &model.HttpError{
			StatusCode: http.StatusInternalServerError,
			StringErr:  err.Error(),
		}
	}

	if upPoints != nil {
		point.Title = upPoints[0].Title
	}

	u.createPointsAction(actor, upPoints, model.ActionPublished)

	return nil
}

func (u *PointUsecase) Unpublish(sess *model.SessionData, ids []int64) error {
	var point model.CollectionPoint

	actor := &model.ActionItem{
		ID:   sess.ID,
		Name: sess.Name,
	}

	point.Status = model.StatusFrozen
	point.UpdatedBy = actor.ID

	upPoints, err := u.repo.UpdateStatus(ids, point)
	if err != nil {
		return &model.HttpError{
			StatusCode: http.StatusInternalServerError,
			StringErr:  err.Error(),
		}
	}
	if upPoints != nil {
		point.Title = upPoints[0].Title
	}

	u.createPointsAction(actor, upPoints, model.ActionUnpublished)

	return nil
}

package city

import (
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"net/http"
)

type repository interface {
	List() ([]model.City, error)
}

type CityUsecase struct {
	repo repository
}

func NewCityUsecase(repo repository) *CityUsecase {
	return &CityUsecase{
		repo: repo,
	}
}

func (u *CityUsecase) List() ([]model.City, error) {
	cities, err := u.repo.List()
	if err != nil {
		return nil, &model.HttpError{
			StatusCode: http.StatusInternalServerError,
			StringErr:  err.Error(),
		}
	}

	return cities, nil
}

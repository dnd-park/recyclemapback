package internal

type Config struct {
	BindAddr    string
	LogLevel    string
	DatabaseURL string
	ClientUrl   string
	SessionKey  string
}

func NewConfig() *Config {
	return &Config{
		BindAddr:    ":9000",
		LogLevel:    "debug",
		DatabaseURL: "host=localhost dbname=recyclemap sslmode=disable port=5432 password=Ryb25vdmRpbWEu user=recycle",
		ClientUrl:   "",
		SessionKey:  "smashaiii",
	}
}

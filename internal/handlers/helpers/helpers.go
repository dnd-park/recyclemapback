package helpers

import (
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"net/url"
	"strconv"
	"strings"
)

const (
	DefaultLimit = 50
	DefaultPage  = 1
)

func GetQueryParams(params *model.PointsParams, values url.Values) error {
	var err error
	filtersUrl, ok := values["fractions"]

	params.Limit = DefaultLimit
	params.Page = DefaultPage
	params.IsAdmin = true

	if ok && len(filtersUrl[0]) != 0 {
		params.IsFilter = true
		params.Filters, err = TranslateToInt(filtersUrl[0])
	}

	limitStr, ok := values["limit"]
	if ok && len(limitStr[0]) != 0 {
		params.Limit, err = strconv.ParseUint(limitStr[0], 10, 32)
		if err != nil {
			return err
		}
	}

	pageStr, ok := values["page"]
	if ok && len(pageStr[0]) != 0 {
		params.Page, err = strconv.ParseUint(pageStr[0], 10, 64)
		if err != nil {
			return err
		}
	}

	return err
}

func GetQueryReqParams(params *model.FiltersReq, values url.Values) error {
	var err error
	filtersUrl, ok := values["fractions"]

	params.Limit = DefaultLimit
	params.Page = DefaultPage

	if ok && len(filtersUrl[0]) != 0 {
		params.IsFilter = true
		params.Filters, err = TranslateToInt(filtersUrl[0])
	}

	limitStr, ok := values["limit"]
	if ok && len(limitStr[0]) != 0 {
		params.Limit, err = strconv.ParseUint(limitStr[0], 10, 32)
		if err != nil {
			return err
		}
	}

	pageStr, ok := values["page"]
	if ok && len(pageStr[0]) != 0 {
		params.Page, err = strconv.ParseUint(pageStr[0], 10, 64)
		if err != nil {
			return err
		}
	}

	status, ok := values["status"]
	if ok {
		if status[0] == model.StatusOnCheck || status[0] == model.StatusDenied || status[0] == model.StatusApproved {
			params.Status = status[0]
		}
	}

	city, ok := values["city"]
	if ok && len(city[0]) != 0 {
		cityID, err := strconv.ParseInt(city[0], 10, 64)
		if err != nil {
			return err
		}

		params.City = cityID
	}

	creator, ok := values["creator"]
	if ok && len(creator[0]) != 0 {
		creatorID, err := strconv.ParseInt(creator[0], 10, 64)
		if err != nil {
			return err
		}

		params.CreatorID = creatorID
	}

	return err
}

func GetQueryComplParams(params *model.ComplaintParams, values url.Values) error {
	var err error

	params.Limit = DefaultLimit
	params.Page = DefaultPage

	limitStr, ok := values["limit"]
	if ok && len(limitStr[0]) != 0 {
		params.Limit, err = strconv.ParseUint(limitStr[0], 10, 32)
		if err != nil {
			return err
		}
	}

	pageStr, ok := values["page"]
	if ok && len(pageStr[0]) != 0 {
		params.Page, err = strconv.ParseUint(pageStr[0], 10, 64)
		if err != nil {
			return err
		}
	}

	email, ok := values["email"]
	if ok && len(email[0]) != 0 {
		params.Email = email[0]
	}

	author, ok := values["author"]
	if ok && len(author[0]) != 0 {
		params.Author, err = strconv.ParseInt(author[0], 10, 64)
		if err != nil {
			return err
		}
	}

	point, ok := values["point"]
	if ok && len(point[0]) != 0 {
		params.PointID, err = strconv.ParseUint(point[0], 10, 32)
		if err != nil {
			return err
		}
	}

	_, ok = values["answered"]
	if ok {
		params.Answered = true
	}

	responder, ok := values["responder"]
	if ok && len(responder[0]) != 0 {
		params.ResponderID, err = strconv.ParseUint(responder[0], 10, 32)
		if err != nil {
			return err
		}
	}

	return nil
}

func GetQueryUsersParams(params *model.UserFilter, values url.Values) error {
	var err error

	params.Limit = DefaultLimit
	params.Page = DefaultPage

	limitStr, ok := values["limit"]
	if ok && len(limitStr[0]) != 0 {
		params.Limit, err = strconv.ParseUint(limitStr[0], 10, 32)
		if err != nil {
			return err
		}
	}

	pageStr, ok := values["page"]
	if ok && len(pageStr[0]) != 0 {
		params.Page, err = strconv.ParseUint(pageStr[0], 10, 64)
		if err != nil {
			return err
		}
	}

	status, ok := values["status"]
	if ok {
		if status[0] == model.BannedStatus || status[0] == model.ActiveStatus {
			params.Status = status[0]
		}
	}

	city, ok := values["city"]
	if ok && len(city[0]) != 0 {
		cityID, err := strconv.ParseInt(city[0], 10, 64)
		if err != nil {
			return err
		}

		params.City = cityID
	}

	types, ok := values["types"]
	if ok && len(types[0]) > 0 {
		params.Types = strings.Split(types[0], ",")
	}

	return err
}

func GetQueryCommentsParams(params *model.CommentParams, values url.Values) error {
	var err error

	params.Limit = DefaultLimit
	params.Page = DefaultPage

	limitStr, ok := values["limit"]
	if ok && len(limitStr[0]) != 0 {
		params.Limit, err = strconv.ParseUint(limitStr[0], 10, 32)
		if err != nil {
			return err
		}
	}

	pageStr, ok := values["page"]
	if ok && len(pageStr[0]) != 0 {
		params.Page, err = strconv.ParseUint(pageStr[0], 10, 64)
		if err != nil {
			return err
		}
	}

	id, ok := values["id"]
	if ok && len(id[0]) != 0 {
		params.ID, err = strconv.ParseInt(pageStr[0], 10, 64)
		if err != nil {
			return err
		}
	}

	author, ok := values["author"]
	if ok && len(author[0]) != 0 {
		params.Author, err = strconv.ParseInt(pageStr[0], 10, 64)
		if err != nil {
			return err
		}
	}

	status, ok := values["status"]
	if ok && len(status[0]) != 0 {
		params.Status = status[0]
	}

	return nil
}
func TranslateToInt(s string) ([]int64, error) {
	fractionIDs := make([]int64, 0, 13)
	fractions := strings.Split(s, ",")
	for _, str := range fractions {
		fractionID, err := strconv.ParseInt(str, 10, 64)
		if err != nil {
			return fractionIDs, err
		}
		fractionIDs = append(fractionIDs, fractionID)
	}
	return fractionIDs, nil
}

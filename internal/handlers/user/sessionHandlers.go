package user

import (
	"encoding/json"
	"github.com/gorilla/sessions"
	"github.com/pkg/errors"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"gitlab.com/dnd-park/recyclemapback/internal/respond"
	"golang.org/x/oauth2"
	"net/http"
)

func (h *UserHandler) HandleSignUp(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			err = errors.Wrapf(err, "HandleSessionCreate<-rBodyClose()")
			h.responder.Error(w, r, http.StatusInternalServerError, err)
		}
	}()

	us := new(model.User)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(us)
	if err != nil {
		err = errors.Wrapf(err, "ERROR : HandleSignUp <- Decode: ")
		err = errors.New("Invalid format data, example")
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	err = h.userUsecase.SignUp(us)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	err = h.SaveSession(w, r, us, CookieMonth)
	if err != nil {
		h.responder.Error(w, r, http.StatusInternalServerError, err)
		return
	}

	h.responder.Respond(w, r, http.StatusCreated, us)
	return
}

func (h *UserHandler) HandleSessionCreate(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			err = errors.Wrapf(err, "HandleSessionCreate<-rBodyClose()")
			h.responder.Error(w, r, http.StatusInternalServerError, err)
		}
	}()

	us := new(model.User)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(us)
	if err != nil {
		err = errors.Wrapf(err, "ERROR : HandleSessionCreate <- Decode: ")
		err = errors.New("Invalid format data")
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	us, err = h.userUsecase.VerifyUser(us)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	err = h.SaveSession(w, r, us, CookieMonth)
	if err != nil {
		h.responder.Error(w, r, http.StatusInternalServerError, err)
		return
	}

	err = h.userUsecase.UpdateLastLogin(us.ID)
	if err != nil {
		h.responder.Error(w, r, http.StatusInternalServerError, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, us)
	return
}

func (h *UserHandler) HandleLogout(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	err := h.SaveSession(w, r, nil, CookieExpired)
	if err != nil {
		h.responder.Error(w, r, http.StatusInternalServerError, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, struct{}{})
	return
}

func (h *UserHandler) SaveSession(w http.ResponseWriter, r *http.Request, user *model.User, maxAge int) error {
	session, err := h.sessionStore.Get(r, respond.SessionName)
	if err != nil {
		err = errors.Wrapf(err, "HandleSessionCreate<-sessionGet()")
		return err
	}

	session.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   maxAge,
		Secure:   true,
		HttpOnly: true,
		SameSite: http.SameSiteNoneMode,
	}

	if user != nil {
		session.Values["user_id"] = user.ID
		session.Values["city"] = user.City
		session.Values["user_type"] = user.UserType
		session.Values["email"] = user.Email
		session.Values["vk_id"] = user.VkID
		session.Values["user_name"] = user.SecondName + " " + user.FirstName
	}

	if err := h.sessionStore.Save(r, w, session); err != nil {
		err = errors.Wrapf(err, "HandleSessionCreate<-sessionSave()")
		return err
	}

	return nil
}

func (h *UserHandler) HandleVkAuth(w http.ResponseWriter, r *http.Request) {
	oauthState := generateStateOauthCookie(w)
	VKOauthConfig := initVkConfig()
	u := VKOauthConfig.AuthCodeURL(oauthState) + "&display=popup"
	h.logger.Infof(` 'path' : '%v', 'CODE' : [%v]`, r.URL.Path, http.StatusTemporaryRedirect)
	http.Redirect(w, r, u, http.StatusTemporaryRedirect)
	return
}

func (h *UserHandler) HandleVkAuthCallback(w http.ResponseWriter, r *http.Request) {
	values := r.URL.Query()

	er, ok := values["error"]
	if ok {
		http.Redirect(w, r, "/?error="+er[0], http.StatusTemporaryRedirect)
		return
	}

	code, ok := values["code"]
	if !ok {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	VKOauthConfig := initVkConfig()

	token, err := VKOauthConfig.Exchange(r.Context(), code[0])
	if err != nil {
		http.Redirect(w, r, "/?error="+err.Error(), http.StatusTemporaryRedirect)
		return
	}

	VkUSer, err := getVkUser(token)

	users, err := h.userUsecase.AuthVk(VkUSer)
	if err != nil {
		http.Redirect(w, r, "/?error=internal error", http.StatusTemporaryRedirect)
		return
	}

	err = h.SaveSession(w, r, &users[0], int(VkUSer.ExpiresIn))
	if err != nil {
		http.Redirect(w, r, "/?error=unable to save session", http.StatusTemporaryRedirect)
		return
	}

	err = h.userUsecase.UpdateLastLogin(users[0].ID)
	if err != nil {
		http.Redirect(w, r, "/?error=unable update last login", http.StatusTemporaryRedirect)
		return
	}

	h.logger.Infof(` 'path' : '%v', 'CODE' : [%v], 'VK_ID' : '%v' `, r.URL.Path, http.StatusFound, users[0].VkID)
	http.Redirect(w, r, "/", http.StatusFound)
	return
}

func getVkUser(token *oauth2.Token) (*model.VkUser, error) {
	VkUser := new(model.VkUser)

	VkUser.AccessToken = token.AccessToken
	email := token.Extra("email")
	if email != nil {
		VkUser.Email = email.(string)
	}
	VkUser.VkID = int64(token.Extra("user_id").(float64))
	VkUser.ExpiresIn = int64(token.Extra("expires_in").(float64))

	response, err := http.Get(model.VKApi + model.UserGetMethod + VkUser.AccessToken)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = response.Body.Close()
	}()

	VkResp := new(model.VkResponseUserApi)
	decoder := json.NewDecoder(response.Body)
	err = decoder.Decode(VkResp)
	if err != nil {
		return nil, err
	}

	VkUser.FirstName = VkResp.Reponses[0].FirstName
	VkUser.LastName = VkResp.Reponses[0].LastName
	VkUser.Avatar = VkResp.Reponses[0].Avatar

	return VkUser, nil
}

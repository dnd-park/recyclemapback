package user

import (
	"encoding/base64"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/vk"
	"math/rand"
	"net/http"
	"os"
	"time"
)

type Result struct {
	IDs []string `json:"ids"`
}

func generateStateOauthCookie(w http.ResponseWriter) string {
	var expiration = time.Now().Add(24 * time.Hour)

	b := make([]byte, 16)
	rand.Read(b)
	state := base64.URLEncoding.EncodeToString(b)
	cookie := http.Cookie{
		Name:    "oauthstate",
		Value:   state,
		Expires: expiration}
	http.SetCookie(w, &cookie)

	return state
}

func initVkConfig() *oauth2.Config {
	return &oauth2.Config{
		RedirectURL:  model.VkRedirect,
		ClientID:     os.Getenv("VK_OAUTH_CLIENT_ID"),
		ClientSecret: os.Getenv("VK_OAUTH_CLIENT_SECRET"),
		Scopes:       []string{"email"},
		Endpoint:     vk.Endpoint,
	}
}

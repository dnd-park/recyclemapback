package user

import (
	"bytes"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/pkg/errors"
	"gitlab.com/dnd-park/recyclemapback/internal/handlers/helpers"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"gitlab.com/dnd-park/recyclemapback/internal/respond"
	complaintU "gitlab.com/dnd-park/recyclemapback/internal/usecases/complaint"
	"gitlab.com/dnd-park/recyclemapback/internal/usecases/request"
	"gitlab.com/dnd-park/recyclemapback/internal/usecases/user"
	"go.uber.org/zap"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

const (
	CookieExpired = -1
	CookieMonth   = 2592000
	ImagesUrl     = "https://recyclemap-images.herokuapp.com"
)

type UserHandler struct {
	userUsecase  user.UserUsecase
	complUsecase complaintU.ComplaintUsecase
	sessionStore sessions.Store
	reqUsecase   request.RequestUsecase
	responder    *respond.Responder
	logger       *zap.SugaredLogger
}

func NewUserHandler(m *mux.Router, private *mux.Router, user user.UserUsecase, req request.RequestUsecase,
	complUsecase complaintU.ComplaintUsecase, logger *zap.SugaredLogger, sessionStore sessions.Store,
	responder *respond.Responder) {
	handler := &UserHandler{
		userUsecase:  user,
		complUsecase: complUsecase,
		reqUsecase:   req,
		responder:    responder,
		logger:       logger,
		sessionStore: sessionStore,
	}

	m.HandleFunc("/user/signup", handler.HandleSignUp).Methods(http.MethodPost, http.MethodOptions)
	m.HandleFunc("/user/login", handler.HandleSessionCreate).Methods(http.MethodPost, http.MethodOptions)
	m.HandleFunc("/vk/auth", handler.HandleVkAuth).Methods(http.MethodGet, http.MethodOptions)
	m.HandleFunc("/vk/auth/callback", handler.HandleVkAuthCallback).Methods(http.MethodGet, http.MethodOptions)
	m.HandleFunc("/user/{id}", handler.HandleGetUser).Methods(http.MethodGet)

	private.HandleFunc("/user/logout", handler.HandleLogout).Methods(http.MethodPost, http.MethodOptions)
	private.HandleFunc("/user/request", handler.HandleNewUserPoint).Methods(http.MethodPost, http.MethodOptions)
	private.HandleFunc("/user/requests", handler.HandleGetOwnReq).Methods(http.MethodGet, http.MethodOptions)
	private.HandleFunc("/user/complaints", handler.HandleGetComplaints).Methods(http.MethodGet, http.MethodOptions)
	private.HandleFunc("/user/requests/{id}/image", handler.HandleAddImages).Methods(http.MethodPost, http.MethodOptions)
	private.HandleFunc("/user/edit", handler.HandleEditUser).Methods(http.MethodPut, http.MethodOptions)
	private.HandleFunc("/user", handler.HandleGetSessionUser).Methods(http.MethodGet)

}

func (h *UserHandler) HandleEditUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			err = errors.Wrapf(err, "HandleEditUser<-rBodyClose()")
			h.responder.Error(w, r, http.StatusInternalServerError, err)
		}
	}()

	sessionData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("no user_id in context"), "HandleEditUser()")
		h.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	us := new(model.User)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(us)
	if err != nil {
		err = errors.Wrapf(err, "ERROR : HandleSignUp <- Decode: ")
		//p.logger.Info(err.Error())
		err = errors.New("Invalid format data, example")
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	us.ID = sessionData.ID

	if err := h.userUsecase.EditUser(us); err != nil {
		err = errors.Wrapf(err, "HandleEditProfile<-Ucase.EditUser()")
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, struct{}{})
	return
}

func (h *UserHandler) HandleGetUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		//err = errors.Wrapf(err, "GetItem<-Atoi(wrong id)")
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	us, err := h.userUsecase.Get(id)
	if err != nil {
		h.responder.Error(w, r, http.StatusNotFound, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, us)
	return
}

func (h *UserHandler) HandleGetSessionUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	sessionData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("no user_id in context"), "HandleEditUser()")
		h.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	us, err := h.userUsecase.Get(sessionData.ID)
	if err != nil {
		h.responder.Error(w, r, http.StatusNotFound, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, us)
	return
}

func (h *UserHandler) HandleNewUserPoint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			err = errors.Wrapf(err, "ERROR : HandleNewUserPoint <- Body.Close")
			h.responder.Error(w, r, http.StatusInternalServerError, err)
			return
		}
	}()

	sessionData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("no user_id in context"), "HandleEditUser()")
		h.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	cp := new(model.CollectionPoint)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(cp)
	if err != nil {
		err = errors.New("Invalid format data, example")
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	cp.CreatedBy = sessionData.ID
	cp.UpdatedBy = sessionData.ID

	id, err := h.reqUsecase.CreateRequest(cp)
	if err != nil {
		rerr := err.(*model.HttpError)
		h.responder.Error(w, r, rerr.StatusCode, rerr)
		return
	}

	h.responder.Respond(w, r, http.StatusCreated, id)
	return
}

func (h *UserHandler) HandleGetOwnReq(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var reqs []model.CollectionPoint
	var params model.FiltersReq
	var err error

	values := r.URL.Query()

	err = helpers.GetQueryReqParams(&params, values)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid filters param"))
		return
	}

	sessionData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("unauthorized"), "HandleGetOwnReq()")
		h.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	params.CreatorID = sessionData.ID

	reqs, err = h.reqUsecase.ListRequests(params)
	if err != nil {
		h.responder.Error(w, r, http.StatusNotFound, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, reqs)
	return
}

func (h *UserHandler) HandleAddImages(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid id"))
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid input data"))
		return
	}

	r.Body = ioutil.NopCloser(bytes.NewReader(body))

	proxyReq, err := http.NewRequest(r.Method, ImagesUrl, bytes.NewReader(body))
	if err != nil {
		h.responder.Error(w, r, http.StatusInternalServerError, errors.New("internal error"))
		return
	}

	proxyReq.Header = make(http.Header)
	for h, val := range r.Header {
		proxyReq.Header[h] = val
	}

	client := &http.Client{Timeout: 4 * time.Second}
	rsp, err := client.Do(proxyReq)
	if err != nil {
		h.responder.Error(w, r, http.StatusInternalServerError, errors.New("internal error"))
		return
	}

	if rsp.StatusCode != http.StatusCreated {
		h.responder.Error(w, r, http.StatusInternalServerError, errors.New("internal error"))
		return
	}

	bodyR, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		h.responder.Error(w, r, http.StatusInternalServerError, errors.New("internal error"))
		return
	}

	var res Result
	if err = json.Unmarshal(bodyR, &res); err != nil {
		h.responder.Error(w, r, http.StatusInternalServerError, errors.New("internal error"))
		return
	}

	imageIDs := make([]string, 0, len(res.IDs))
	for _, id := range res.IDs {
		imageIDs = append(imageIDs, ImagesUrl+"/"+id)
	}

	if err := h.reqUsecase.AddImages(id, imageIDs); err != nil {
		httpError := err.(*model.HttpError)
		h.responder.Error(w, r, httpError.StatusCode, errors.New("cant add images"))
		return
	}
	h.responder.Respond(w, r, http.StatusOK, struct{}{})
}

func (h *UserHandler) HandleGetComplaints(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var params model.ComplaintParams
	var err error

	values := r.URL.Query()

	err = helpers.GetQueryComplParams(&params, values)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid filters param"))
		return
	}
	sessionData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("unauthorized"), "HandleGetOwnReq()")
		h.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	params.Author = sessionData.ID

	complaints, err := h.complUsecase.List(params)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
	}

	h.responder.Respond(w, r, http.StatusOK, complaints)
	return
}

package grade

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"gitlab.com/dnd-park/recyclemapback/internal/respond"
	"go.uber.org/zap"
	"net/http"
)

type usecase interface {
	Create(userID int64, grades []model.Grade) ([]int64, error)
}

type Handler struct {
	ucase        usecase
	logger       *zap.SugaredLogger
	sessionStore sessions.Store
	responder    *respond.Responder
}

type Result struct {
	IDs []int64 `json:"updatedPointsIds"`
}

type Grades struct {
	Grades []model.Grade `json:"grades"`
}

func NewGradeHandler(m *mux.Router, logger *zap.SugaredLogger,
	sessionStore sessions.Store, responder *respond.Responder, ucase usecase) {
	handler := &Handler{
		ucase:        ucase,
		logger:       logger,
		sessionStore: sessionStore,
		responder:    responder,
	}

	m.HandleFunc("/grade", handler.Default).Methods(http.MethodPut, http.MethodOptions)
}

func (h *Handler) Default(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			return
		}
	}()

	var grades Grades
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&grades)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid input data"))
		return
	}

	sessionData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		h.responder.Error(w, r, http.StatusUnauthorized, errors.New("login to continue"))
		return
	}

	userId := sessionData.ID

	ids, err := h.ucase.Create(userId, grades.Grades)
	if err != nil {
		h.responder.Error(w, r, http.StatusInternalServerError, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, Result{IDs: ids})
}

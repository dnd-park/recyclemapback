package point

import (
	"strconv"
	"strings"
)

func (p *PointHandler) translateToInt(s string) ([]int64, error) {
	fractionIDs := make([]int64, 0, 13)
	fractions := strings.Split(s, ",")
	for _, str := range fractions {
		fractionID, err := strconv.ParseInt(str, 10, 64)
		if err != nil {
			return fractionIDs, err
		}
		fractionIDs = append(fractionIDs, fractionID)
	}
	return fractionIDs, nil
}

package point

import (
	"bytes"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/pkg/errors"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"gitlab.com/dnd-park/recyclemapback/internal/respond"
	complaintU "gitlab.com/dnd-park/recyclemapback/internal/usecases/complaint"
	"gitlab.com/dnd-park/recyclemapback/internal/usecases/point"
	"go.uber.org/zap"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"
)

const (
	ImagesUrl = "https://recyclemap-images.herokuapp.com"
)

type PointHandler struct {
	pointUsecase point.PointUsecase
	complUsecase complaintU.ComplaintUsecase
	logger       *zap.SugaredLogger
	sessionStore sessions.Store
	responder    *respond.Responder
}

type Result struct {
	IDs []string `json:"ids"`
}

func NewPointHandler(m *mux.Router, moderator *mux.Router, point point.PointUsecase, logger *zap.SugaredLogger,
	sessionStore sessions.Store, compl complaintU.ComplaintUsecase, responder *respond.Responder) {
	handler := &PointHandler{
		pointUsecase: point,
		logger:       logger,
		sessionStore: sessionStore,
		complUsecase: compl,
		responder:    responder,
	}

	m.HandleFunc("/", handler.Default).Methods(http.MethodGet)
	m.HandleFunc("/point/{id}", handler.HandleGetPoint).Methods(http.MethodGet)
	m.HandleFunc("/points", handler.HandleGetListPoints).Methods(http.MethodGet)
	m.HandleFunc("/points/near", handler.HandleGetNearPoints).Methods(http.MethodPost)
	m.HandleFunc("/points/full", handler.HandleFullPoints).Methods(http.MethodGet)

	moderator.HandleFunc("/points/{id}/image", handler.HandleAddImages).Methods(http.MethodPost, http.MethodOptions)
	moderator.HandleFunc("/point/add", handler.HandleAddPoint).Methods(http.MethodPost, http.MethodOptions)
	moderator.HandleFunc("/points/add", handler.HandleAddPoints).Methods(http.MethodPost, http.MethodOptions)
	moderator.HandleFunc("/points/{id}/complaint", handler.HandleNewComplaint).Methods(http.MethodPost, http.MethodOptions)
	moderator.HandleFunc("/points/update/status/{status}", handler.HandleChangeStatus).Methods(http.MethodPut, http.MethodOptions)
	moderator.HandleFunc("/points/update", handler.HandleUpdatePoint).Methods(http.MethodPut, http.MethodOptions)
}

func (p *PointHandler) Default(w http.ResponseWriter, r *http.Request) {
	p.responder.Respond(w, r, http.StatusOK, "hello from server")
}

func (p *PointHandler) HandleGetPoint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		httpError := err.(*model.HttpError)
		p.responder.Error(w, r, httpError.StatusCode, errors.New("invalid data"))
		return
	}

	po, err := p.pointUsecase.Get(id)
	if err != nil {
		httpError := err.(*model.HttpError)
		p.responder.Error(w, r, httpError.StatusCode, errors.New("cant get point"))
		return
	}

	p.responder.Respond(w, r, http.StatusOK, po)
}

func (p *PointHandler) HandleGetListPoints(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var points []model.CollectionPoint
	var params model.PointsParams
	var err error

	values := r.URL.Query()
	filtersUrl, ok := values["filters"]

	if ok {
		params.IsFilter = true
		params.Filters, err = p.translateToInt(filtersUrl[0])
	}

	if err != nil {
		p.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid filters param"))
		return
	}

	points, err = p.pointUsecase.List(params)
	if err != nil {
		httpError := err.(*model.HttpError)
		p.responder.Error(w, r, httpError.StatusCode, errors.New("cant get points"))
		return
	}

	p.responder.Respond(w, r, http.StatusOK, points)
}

func (p *PointHandler) HandleAddPoint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			return
		}
	}()

	cp := new(model.CollectionPoint)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(cp)
	if err != nil {
		p.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid input data"))
		return
	}

	sessData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("unauthorized"), "ChangeRules()")
		p.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	id, err := p.pointUsecase.CreatePoint(sessData, cp)
	if err != nil {
		httpError := err.(*model.HttpError)
		p.responder.Error(w, r, httpError.StatusCode, errors.New("cant create point"))
		return
	}

	p.responder.Respond(w, r, http.StatusCreated, map[string]int64{"id": id})
	return
}

func (p *PointHandler) HandleGetNearPoints(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var points []model.CollectionPoint
	var err error

	geopoint := new(model.GeoPoint)
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(geopoint)
	if err != nil {
		p.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid input data"))
		return
	}

	points, err = p.pointUsecase.ListNearest(*geopoint)
	if err != nil {
		httpError := err.(*model.HttpError)
		p.responder.Error(w, r, httpError.StatusCode, errors.New("cant get nearest points"))
		return
	}

	p.responder.Respond(w, r, http.StatusOK, points)
}

func (p *PointHandler) HandleFullPoints(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var limit uint64
	var page uint64
	var err error

	values := r.URL.Query()
	limitStr, ok := values["limit"]
	if ok {
		if len(limitStr) == 0 {
			p.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid limit"))
			return
		}
		limit, err = strconv.ParseUint(limitStr[0], 10, 64)
		if err != nil {
			p.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid limit"))
			return
		}
	}

	pageStr, ok := values["page"]
	if ok {
		if len(pageStr) == 0 {
			p.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid page"))
			return
		}
		page, err = strconv.ParseUint(pageStr[0], 10, 64)
		if err != nil {
			p.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid page"))
			return
		}
	}

	if page < 1 {
		p.responder.Error(w, r, http.StatusBadRequest, errors.New("page must be greater than 0"))
		return
	}

	points, err := p.pointUsecase.ListFull(limit, page)
	if err != nil {
		httpError := err.(*model.HttpError)
		log.Println(err)
		p.responder.Error(w, r, httpError.StatusCode, errors.New("cant get nearest points"))
		return
	}

	p.responder.Respond(w, r, http.StatusOK, points)
}

func (p *PointHandler) HandleAddImages(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		p.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid id"))
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		p.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid input data"))
		return
	}

	r.Body = ioutil.NopCloser(bytes.NewReader(body))

	proxyReq, err := http.NewRequest(r.Method, ImagesUrl, bytes.NewReader(body))
	if err != nil {
		p.responder.Error(w, r, http.StatusInternalServerError, errors.New("internal error"))
		return
	}

	proxyReq.Header = make(http.Header)
	for h, val := range r.Header {
		proxyReq.Header[h] = val
	}

	client := &http.Client{Timeout: 4 * time.Second}
	rsp, err := client.Do(proxyReq)
	if err != nil {
		p.responder.Error(w, r, http.StatusInternalServerError, errors.New("internal error"))
		return
	}

	if rsp.StatusCode != http.StatusCreated {
		p.responder.Error(w, r, http.StatusInternalServerError, errors.New("internal error"))
		return
	}

	bodyR, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		p.responder.Error(w, r, http.StatusInternalServerError, errors.New("internal error"))
		return
	}

	var res Result
	if err = json.Unmarshal(bodyR, &res); err != nil {
		p.responder.Error(w, r, http.StatusInternalServerError, errors.New("internal error"))
		return
	}

	imageIDs := make([]string, 0, len(res.IDs))
	for _, id := range res.IDs {
		imageIDs = append(imageIDs, ImagesUrl+"/"+id)
	}

	if err := p.pointUsecase.AddImages(id, imageIDs); err != nil {
		httpError := err.(*model.HttpError)
		p.responder.Error(w, r, httpError.StatusCode, errors.New("cant add images"))
		return
	}

	p.responder.Respond(w, r, http.StatusOK, struct{}{})
}

type Points struct {
	Points []model.CollectionPoint `json:"points"`
}

func (p *PointHandler) HandleAddPoints(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			return
		}
	}()

	var points Points
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&points)
	if err != nil {
		p.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid input"))
		return
	}

	sessData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("unauthorized"), "ChangeRules()")
		p.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	ids, err := p.pointUsecase.CreatePoints(sessData, points.Points)
	if err != nil {
		httpError := err.(*model.HttpError)
		p.responder.Error(w, r, httpError.StatusCode, httpError)
		return
	}

	p.responder.Respond(w, r, http.StatusCreated, map[string][]int64{"ids": ids})
}

type PointsIDs struct {
	IDs []int64 `json:"ids"`
}

func (p *PointHandler) HandleChangeStatus(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			return
		}
	}()

	points := new(PointsIDs)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(points)
	if err != nil {
		p.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid input data"))
		return
	}

	sessionData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		p.responder.Error(w, r, http.StatusUnauthorized, errors.New("login to continue"))
		return
	}

	vars := mux.Vars(r)
	status := vars["status"]

	if status == "publish" {
		if err := p.pointUsecase.Publish(sessionData, points.IDs); err != nil {
			httpError := err.(*model.HttpError)
			p.responder.Error(w, r, httpError.StatusCode, httpError)
			return
		}
	} else if status == "unpublish" {
		if err := p.pointUsecase.Unpublish(sessionData, points.IDs); err != nil {
			httpError := err.(*model.HttpError)
			p.responder.Error(w, r, httpError.StatusCode, httpError)
			return
		}
	} else {
		p.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid request"))
		return
	}

	p.responder.Respond(w, r, http.StatusOK, struct{}{})
}

type UpdateOpts struct {
	IDs   []int64               `json:"ids"`
	Point model.CollectionPoint `json:"point"`
}

func (p *PointHandler) HandleUpdatePoint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	defer func() {
		if err := r.Body.Close(); err != nil {
			return
		}
	}()

	updateOpts := new(UpdateOpts)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(updateOpts)
	if err != nil {
		p.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid input data"))
		return
	}

	sessionData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		p.responder.Error(w, r, http.StatusUnauthorized, errors.New("login to continue"))
		return
	}

	err = p.pointUsecase.Update(sessionData, updateOpts.IDs, &updateOpts.Point)
	if err != nil {
		p.responder.Error(w, r, http.StatusInternalServerError, errors.New("can't update point"))
		return
	}

	p.responder.Respond(w, r, http.StatusOK, struct{}{})
}

func (p *PointHandler) HandleNewComplaint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			return
		}
	}()

	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		p.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid id"))
		return
	}

	sessData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("unauthorized"), "ChangeRules()")
		p.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	complaint := new(model.Complaint)
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(complaint)
	if err != nil {
		p.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid input data"))
		return
	}

	complaint.PointID = id
	complaint.Email = sessData.Email
	complaint.Author = sessData.ID

	err = p.complUsecase.Create(complaint)
	if err != nil {
		p.responder.Error(w, r, http.StatusInternalServerError, errors.New("cant create complaint"))
		return
	}

	p.responder.Respond(w, r, http.StatusCreated, complaint.ID)
	return
}

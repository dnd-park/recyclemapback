package admin

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/pkg/errors"
	"gitlab.com/dnd-park/recyclemapback/internal/handlers/helpers"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"gitlab.com/dnd-park/recyclemapback/internal/respond"
	"gitlab.com/dnd-park/recyclemapback/internal/usecases/admin"
	complaintU "gitlab.com/dnd-park/recyclemapback/internal/usecases/complaint"
	pointU "gitlab.com/dnd-park/recyclemapback/internal/usecases/point"
	"gitlab.com/dnd-park/recyclemapback/internal/usecases/request"
	"gitlab.com/dnd-park/recyclemapback/internal/usecases/user"
	"go.uber.org/zap"
	"net/http"
	"strconv"
)

type AdminHandler struct {
	adminUsecase     admin.AdminUsecase
	userUsecase      user.UserUsecase
	logger           *zap.SugaredLogger
	sessionStore     sessions.Store
	reqUsecase       request.RequestUsecase
	pointUsecase     pointU.PointUsecase
	complaintUsecase complaintU.ComplaintUsecase
	responder        *respond.Responder
}

func NewAdminHandler(admin *mux.Router, adminU admin.AdminUsecase, userU user.UserUsecase,
	requestUC request.RequestUsecase, pointU pointU.PointUsecase, complUC complaintU.ComplaintUsecase,
	logger *zap.SugaredLogger, sessionStore sessions.Store, responder *respond.Responder) {
	handler := &AdminHandler{
		adminUsecase:     adminU,
		pointUsecase:     pointU,
		userUsecase:      userU,
		complaintUsecase: complUC,
		logger:           logger,
		sessionStore:     sessionStore,
		reqUsecase:       requestUC,
		responder:        responder,
	}

	admin.HandleFunc("/", handler.Default).Methods(http.MethodPost, http.MethodOptions)
	admin.HandleFunc("/privilege/grant/{id}", handler.ChangeRules).Methods(http.MethodPost, http.MethodOptions)
	admin.HandleFunc("/requests/{id}", handler.UpdateRequest).Methods(http.MethodPut, http.MethodOptions)
	admin.HandleFunc("/requests", handler.GetRequests).Methods(http.MethodGet, http.MethodOptions)
	admin.HandleFunc("/users", handler.GetUsers).Methods(http.MethodGet, http.MethodOptions)
	admin.HandleFunc("/points", handler.HandleGetFullPoints).Methods(http.MethodGet, http.MethodOptions)
	admin.HandleFunc("/complaints", handler.HandleGetComplaints).Methods(http.MethodGet, http.MethodOptions)
	admin.HandleFunc("/complaint/{id}/answer", handler.HandleAnswerComplaint).Methods(http.MethodPost, http.MethodOptions)
	admin.HandleFunc("/requests/{id}/approve", handler.ApproveRequest).Methods(http.MethodPut, http.MethodOptions)
	admin.HandleFunc("/requests/{id}/deny", handler.DenyRequest).Methods(http.MethodPut, http.MethodOptions)

}

func (h *AdminHandler) Default(w http.ResponseWriter, r *http.Request) {
	h.responder.Respond(w, r, http.StatusOK, "hello admin from server")
}

func (h *AdminHandler) ChangeRules(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var cityID int64

	defer func() {
		if err := r.Body.Close(); err != nil {
			err = errors.Wrapf(err, "ChangeRules<-rBodyClose()")
			h.responder.Error(w, r, http.StatusInternalServerError, err)
		}
	}()

	sessData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("unauthorized"), "ChangeRules()")
		h.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	newType := model.AdminType

	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	values := r.URL.Query()
	moderCity, ok := values["moderator"]
	if ok {
		cityID, err = strconv.ParseInt(moderCity[0], 10, 64)
		if err != nil {
			h.responder.Error(w, r, http.StatusBadRequest, errors.New("Неправильный формат поля moderator, должно быть int"))
		}
		newType = model.ModeratorType
	}

	if err := h.adminUsecase.NewAdmin(sessData, id, newType, cityID); err != nil {
		err = errors.Wrapf(err, "ChangeRules<-Ucase.NewAdmin()")
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, struct{}{})
	return
}

func (h *AdminHandler) UpdateRequest(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			err = errors.Wrapf(err, "ChangeRules<-rBodyClose()")
			h.responder.Error(w, r, http.StatusInternalServerError, err)
		}
	}()

	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	sessData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("unauthorized"), "ChangeRules()")
		h.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	cp := new(model.CollectionPoint)
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(cp)
	if err != nil {
		err = errors.New("Invalid format data, example")
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	cp.ID = id
	cp.UpdatedBy = sessData.ID

	err = h.reqUsecase.Update(cp)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, struct{}{})
	return
}

func (h *AdminHandler) ApproveRequest(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			err = errors.Wrapf(err, "ChangeRules<-rBodyClose()")
			h.responder.Error(w, r, http.StatusInternalServerError, err)
		}
	}()

	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	sessData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("unauthorized"), "ChangeRules()")
		h.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	err = h.adminUsecase.ApproveRequest(sessData, id)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, struct{}{})
	return
}

func (h *AdminHandler) DenyRequest(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			err = errors.Wrapf(err, "ChangeRules<-rBodyClose()")
			h.responder.Error(w, r, http.StatusInternalServerError, err)
		}
	}()

	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	sessData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("unauthorized"), "ChangeRules()")
		h.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	err = h.adminUsecase.DenyRequest(sessData, id)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, struct{}{})
	return
}

func (h *AdminHandler) GetRequests(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var reqs []model.CollectionPoint
	var params model.FiltersReq
	var err error

	values := r.URL.Query()

	err = helpers.GetQueryReqParams(&params, values)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid filters param"))
		return
	}

	reqs, err = h.reqUsecase.ListRequests(params)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
	}

	h.responder.Respond(w, r, http.StatusOK, reqs)
	return
}

func (h *AdminHandler) HandleGetFullPoints(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var points []model.CollectionPoint
	var params model.PointsParams
	var err error

	values := r.URL.Query()

	err = helpers.GetQueryParams(&params, values)

	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid filters param"))
		return
	}

	points, err = h.pointUsecase.ListFullAdmin(params)
	if err != nil {
		httpError := err.(*model.HttpError)
		h.responder.Error(w, r, httpError.StatusCode, errors.New("cant get points"))
		return
	}

	h.responder.Respond(w, r, http.StatusOK, points)
	return
}

func (h *AdminHandler) HandleGetComplaints(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var params model.ComplaintParams
	var err error

	values := r.URL.Query()

	err = helpers.GetQueryComplParams(&params, values)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid filters param"))
		return
	}

	complaints, err := h.complaintUsecase.List(params)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
	}

	h.responder.Respond(w, r, http.StatusOK, complaints)
	return
}

func (h *AdminHandler) HandleAnswerComplaint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			err = errors.Wrapf(err, "ChangeRules<-rBodyClose()")
			h.responder.Error(w, r, http.StatusInternalServerError, err)
		}
	}()

	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	sessData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("unauthorized"), "ChangeRules()")
		h.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	ca := new(model.ComplaintAnswer)
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(ca)
	if err != nil {
		err = errors.New("Invalid format data, example")
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	err = h.complaintUsecase.Answer(id, ca.Answer, sessData.ID)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
	}

	h.responder.Respond(w, r, http.StatusOK, struct{}{})
	return
}

func (h *AdminHandler) GetUsers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var users []model.User
	var params model.UserFilter
	var err error

	values := r.URL.Query()

	err = helpers.GetQueryUsersParams(&params, values)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid filters param"))
		return
	}

	users, err = h.userUsecase.List(&params)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
	}

	h.responder.Respond(w, r, http.StatusOK, users)
	return
}

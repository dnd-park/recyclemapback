package city

import (
	"errors"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"gitlab.com/dnd-park/recyclemapback/internal/respond"
	"go.uber.org/zap"
	"net/http"
)

type usecase interface {
	List() ([]model.City, error)
}

type Handler struct {
	usecase      usecase
	logger       *zap.SugaredLogger
	sessionStore sessions.Store
	responder    *respond.Responder
}

func NewCityHandler(m *mux.Router, usecase usecase, logger *zap.SugaredLogger, sessionStore sessions.Store,
	responder *respond.Responder) {
	handler := &Handler{
		usecase:      usecase,
		logger:       logger,
		sessionStore: sessionStore,
		responder:    responder,
	}

	m.HandleFunc("/cities", handler.HandleList).Methods(http.MethodGet)
}

func (h *Handler) HandleList(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	cities, err := h.usecase.List()
	if err != nil {
		httpError := err.(*model.HttpError)
		h.responder.Error(w, r, httpError.StatusCode, errors.New("cant get point"))
		return
	}

	h.responder.Respond(w, r, http.StatusOK, map[string][]model.City{"cities": cities})
}

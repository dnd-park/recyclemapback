package favorite

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/pkg/errors"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"gitlab.com/dnd-park/recyclemapback/internal/respond"
	"go.uber.org/zap"
	"net/http"
)

type services interface {
	Create(userId int64, ids []int64) error
	Delete(userId int64, ids []int64) error
	List(userId int64) ([]model.CollectionPoint, error)
}

type Handler struct {
	services     services
	logger       *zap.SugaredLogger
	sessionStore sessions.Store
	responder    *respond.Responder
}

func NewFavoriteHandler(m *mux.Router, services services, logger *zap.SugaredLogger, sessionStore sessions.Store,
	responder *respond.Responder) {
	handler := &Handler{
		services:     services,
		logger:       logger,
		sessionStore: sessionStore,
		responder:    responder,
	}

	m.HandleFunc("/favorite", handler.HandleAddFavorites).Methods(http.MethodPost, http.MethodOptions)
	m.HandleFunc("/favorite/delete", handler.HandleDeleteFavorites).Methods(http.MethodPost, http.MethodOptions)
	m.HandleFunc("/favorite", handler.HandleListFavorites).Methods(http.MethodGet, http.MethodOptions)
}

type FavIDs struct {
	IDs []int64 `json:"ids"`
}

func (h *Handler) HandleAddFavorites(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			return
		}
	}()

	favs := new(FavIDs)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(favs)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid input data"))
		return
	}

	sessionData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		h.responder.Error(w, r, http.StatusUnauthorized, errors.New("login to continue"))
		return
	}

	userId := sessionData.ID

	if err := h.services.Create(userId, favs.IDs); err != nil {
		h.responder.Error(w, r, http.StatusInternalServerError, errors.New("cant add to favorites"))
		return
	}

	h.responder.Respond(w, r, http.StatusOK, struct{}{})
}

func (h *Handler) HandleDeleteFavorites(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			return
		}
	}()

	favs := new(FavIDs)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(favs)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid input data"))
		return
	}

	sessionData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {

		h.responder.Error(w, r, http.StatusUnauthorized, errors.New("login to continue"))
		return
	}

	userId := sessionData.ID

	if err := h.services.Delete(userId, favs.IDs); err != nil {
		h.responder.Error(w, r, http.StatusInternalServerError, errors.New("cant delete from favorites"))
		return
	}

	h.responder.Respond(w, r, http.StatusOK, struct{}{})
}

func (h *Handler) HandleListFavorites(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	sessionData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		h.responder.Error(w, r, http.StatusUnauthorized, errors.New("login to continue"))
		return
	}

	userId := sessionData.ID

	points, err := h.services.List(userId)
	if err != nil {
		httpError := err.(*model.HttpError)
		h.responder.Error(w, r, httpError.StatusCode, httpError)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, points)
}

package action

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"gitlab.com/dnd-park/recyclemapback/internal/respond"
	"go.uber.org/zap"
	"net/http"
)

type usecase interface {
	List(params model.ActionParams) ([]model.Action, error)
}

type Handler struct {
	ucase        usecase
	logger       *zap.SugaredLogger
	sessionStore sessions.Store
	responder    *respond.Responder
}

func NewActionHandler(m *mux.Router, logger *zap.SugaredLogger,
	sessionStore sessions.Store, responder *respond.Responder, ucase usecase) {
	handler := &Handler{
		ucase:        ucase,
		logger:       logger,
		sessionStore: sessionStore,
		responder:    responder,
	}

	m.HandleFunc("/actions", handler.HandleList).Methods(http.MethodPost, http.MethodOptions)
}

func (h *Handler) HandleList(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			return
		}
	}()

	var params model.ActionParams
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&params)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid input data"))
		return
	}

	actions, err := h.ucase.List(params)
	if err != nil {
		h.responder.Error(w, r, http.StatusInternalServerError, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, map[string][]model.Action{"actions": actions})
}

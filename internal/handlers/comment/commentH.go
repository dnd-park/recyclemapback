package comment

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/pkg/errors"
	"gitlab.com/dnd-park/recyclemapback/internal/handlers/helpers"
	"gitlab.com/dnd-park/recyclemapback/internal/model"
	"gitlab.com/dnd-park/recyclemapback/internal/respond"
	"gitlab.com/dnd-park/recyclemapback/internal/usecases/comment"
	complaintU "gitlab.com/dnd-park/recyclemapback/internal/usecases/complaint"
	pointU "gitlab.com/dnd-park/recyclemapback/internal/usecases/point"
	"gitlab.com/dnd-park/recyclemapback/internal/usecases/request"
	"gitlab.com/dnd-park/recyclemapback/internal/usecases/user"
	"go.uber.org/zap"
	"log"
	"net/http"
	"strconv"
)

type CommentHandler struct {
	commentUsecase   comment.CommentUsecase
	userUsecase      user.UserUsecase
	logger           *zap.SugaredLogger
	sessionStore     sessions.Store
	reqUsecase       request.RequestUsecase
	pointUsecase     pointU.PointUsecase
	complaintUsecase complaintU.ComplaintUsecase
	responder        *respond.Responder
}

func NewCommentHandler(m *mux.Router, private *mux.Router, admin *mux.Router, commentUsecase comment.CommentUsecase,
	userU user.UserUsecase, pointU pointU.PointUsecase, logger *zap.SugaredLogger, sessionStore sessions.Store,
	responder *respond.Responder) {
	handler := &CommentHandler{
		commentUsecase: commentUsecase,
		pointUsecase:   pointU,
		userUsecase:    userU,
		logger:         logger,
		sessionStore:   sessionStore,
		responder:      responder,
	}

	private.HandleFunc("/point/{id}/comment", handler.HandleCreateComment).Methods(http.MethodPost, http.MethodOptions)
	private.HandleFunc("/comment/{id}/delete", handler.HandleDeleteComment).Methods(http.MethodPost, http.MethodOptions)
	private.HandleFunc("/comments", handler.HandleGetOwnComments).Methods(http.MethodGet, http.MethodOptions)

	m.HandleFunc("/point/{id}/comments", handler.HandleGetComments).Methods(http.MethodGet, http.MethodOptions)
	admin.HandleFunc("/comments", handler.HandleGetAllComments).Methods(http.MethodGet, http.MethodOptions)

}

func (h *CommentHandler) HandleCreateComment(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			return
		}
	}()

	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid id"))
		return
	}

	sessData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("unauthorized"), "ChangeRules()")
		h.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	cmnt := new(model.Comment)
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(cmnt)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid input data"))
		return
	}

	cmnt.PointID = id
	cmnt.Author.ID = sessData.ID

	err = h.commentUsecase.Create(cmnt)
	log.Print(err)
	if err != nil {
		h.responder.Error(w, r, http.StatusInternalServerError, errors.New("cant create comment"))
		return
	}

	h.responder.Respond(w, r, http.StatusCreated, cmnt.ID)
	return
}

func (h *CommentHandler) HandleDeleteComment(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			return
		}
	}()

	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid id"))
		return
	}

	sessData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("unauthorized"), "ChangeRules()")
		h.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	err = h.commentUsecase.Delete(id, sessData.ID)
	if err != nil {
		h.responder.Error(w, r, http.StatusNotFound, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, struct{}{})
	return
}

func (h *CommentHandler) HandleGetComments(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			return
		}
	}()

	var params model.CommentParams

	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid id"))
		return
	}

	values := r.URL.Query()
	err = helpers.GetQueryCommentsParams(&params, values)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid filters param"))
		return
	}

	params.PointID = id
	params.Status = model.StatusPublished

	comments, err := h.commentUsecase.List(params)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, comments)
	return
}

func (h *CommentHandler) HandleGetAllComments(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			return
		}
	}()

	var params model.CommentParams

	values := r.URL.Query()
	err := helpers.GetQueryCommentsParams(&params, values)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid filters param"))
		return
	}

	comments, err := h.commentUsecase.List(params)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, comments)
	return
}

func (h *CommentHandler) HandleGetOwnComments(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	defer func() {
		if err := r.Body.Close(); err != nil {
			return
		}
	}()

	var params model.CommentParams

	sessData, ok := r.Context().Value(respond.CtxKeyUser).(*model.SessionData)
	if !ok {
		err := errors.Wrapf(errors.New("unauthorized"), "ChangeRules()")
		h.responder.Error(w, r, http.StatusUnauthorized, err)
		return
	}

	values := r.URL.Query()
	err := helpers.GetQueryCommentsParams(&params, values)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, errors.New("invalid filters param"))
		return
	}

	params.Author = sessData.ID

	comments, err := h.commentUsecase.List(params)
	if err != nil {
		h.responder.Error(w, r, http.StatusBadRequest, err)
		return
	}

	h.responder.Respond(w, r, http.StatusOK, comments)
	return
}

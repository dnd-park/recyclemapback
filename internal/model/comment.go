package model

import "time"

type Author struct {
	ID         int64  `json:"id"`
	FirstName  string `json:"firstName"`
	SecondName string `json:"secondName"`
}

type Comment struct {
	ID          int64     `json:"id"`
	Comment     string    `json:"comment"`
	PointID     int64     `json:"point_id"`
	Status      string    `json:"status"`
	Author      Author    `json:"author"`
	LastUpdated time.Time `json:"updated, omitempty" valid:"-"`
	Created     time.Time `json:"created, omitempty" valid:"-"`
}

type CommentParams struct {
	Status  string
	ID      int64
	Author  int64
	Page    uint64
	Limit   uint64
	PointID int64
}

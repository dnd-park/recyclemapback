package model

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"
)

type KitchenJSONTime struct {
	time.Time
}

func (t *KitchenJSONTime) UnmarshalJSON(b []byte) (err error) {
	s := strings.Trim(string(b), "\"")
	if s == "null" {
		t.Time = time.Time{}
		return
	}
	t.Time, err = time.Parse(time.Kitchen, s)
	if err != nil {
		t.Time, err = time.Parse(time.RFC3339, s)
	}
	return
}

func (t *KitchenJSONTime) MarshalJSON() ([]byte, error) {
	timeStr := fmt.Sprintf("%q", t.Time.Format(time.Kitchen))
	//log.Println(timeStr)
	return []byte(timeStr), nil
}

type RFC850JSONTime struct {
	time.Time
}

func (t *RFC850JSONTime) UnmarshalJSON(b []byte) (err error) {
	s := strings.Trim(string(b), "\"")
	if s == "null" {
		t.Time = time.Time{}
		return
	}
	t.Time, err = time.Parse(time.RFC850, s)
	if err != nil {
		t.Time, err = time.Parse(time.RFC3339, s)
	}

	return
}

func (t *RFC850JSONTime) MarshalJSON() ([]byte, error) {
	timeStr := fmt.Sprintf("%q", t.Time.Format(time.RFC850))
	return []byte(timeStr), nil
}

type Weekday struct {
	ID        int64           `json:"-"`
	Name      string          `json:"name"`
	StartTime KitchenJSONTime `json:"startTime"`
	EndTime   KitchenJSONTime `json:"endTime"`
	Work      bool            `json:"work"`
}

type Timetable struct {
	ID                int64           `json:"-"`
	Point             int64           `json:"point,omitempty"`
	Weekdays          []Weekday       `json:"weekdays"`
	IsRoundTheClock   bool            `json:"isRoundTheClock"`
	IsBreak           bool            `json:"isBreak"`
	IsCampaign        bool            `json:"isCampaign"`
	IsFloating        bool            `json:"isFloating"`
	BreakStartTime    KitchenJSONTime `json:"breakStartTime"`
	BreakEndTime      KitchenJSONTime `json:"breakEndTime"`
	CampaignStartTime RFC850JSONTime  `json:"campaignStartTime"`
	CampaignEndTime   RFC850JSONTime  `json:"campaignEndTime"`
	WeekdaysJson      []string        `json:"-"`
}

func (t *Timetable) WeekdaysToJson() error {
	t.WeekdaysJson = make([]string, 0, 7)

	if len(t.Weekdays) != 7 {
		return errors.New("not all days")
	}

	for i, day := range t.Weekdays {
		day.ID = int64(i)
		dayJson, err := json.Marshal(day)
		if err != nil {
			return err
		}
		t.WeekdaysJson = append(t.WeekdaysJson, string(dayJson))
	}
	return nil
}

func (t *Timetable) WeekdaysFromJson() error {
	t.Weekdays = make([]Weekday, 0, 7)

	if len(t.WeekdaysJson) != 7 {
		return errors.New("not all days")
	}

	for _, dayJson := range t.WeekdaysJson {
		var day Weekday
		err := json.Unmarshal([]byte(dayJson), &day)
		if err != nil {
			return err
		}
		t.Weekdays = append(t.Weekdays, day)
	}
	return nil
}

func (t *Timetable) Validate() error {
	// TODO: сделать проверку промежутков времени
	return nil
}

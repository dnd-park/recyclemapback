package model

import "time"

const (
	ActionRequest = "request"
	ActionPoint   = "point"
	ActionUser    = "user"

	ActionApproved       = "approved"
	ActionDenied         = "denied"
	ActionUpdated        = "updated"
	ActionPublished      = "published"
	ActionUnpublished    = "unpublished"
	ActionCreated        = "created"
	ActionGrantModerator = "grant_moderator"
	ActionGrantAdmin     = "grant_admin"
)

type ActionItem struct {
	ID   int64  `json:"id"`
	Type string `json:"type,omitempty"`
	Name string `json:"name"`
}

type Action struct {
	ID        int64      `json:"id"`
	Title     string     `json:"title"`
	Action    string     `json:"action"`
	Actor     ActionItem `json:"actor"`
	Item      ActionItem `json:"item"`
	City      int64      `json:"city"`
	CreatedAt time.Time  `json:"createdAt"`
}

type ActionParams struct {
	Items     []ActionItem `json:"items"`
	Actors    []int64      `json:"actors"`
	Types     []string     `json:"types"`
	Actions   []string     `json:"actions"`
	Limit     uint64       `json:"limit"`
	Page      uint64       `json:"page"`
	Desc      bool         `json:"desc"`
	StartDate time.Time    `json:"startDate"`
	EndDate   time.Time    `json:"endDate"`
	City      int64        `json:"city"`
}

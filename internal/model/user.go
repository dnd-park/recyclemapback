package model

import (
	"golang.org/x/crypto/bcrypt"
	"time"
)

const (
	AdminType     = "admin"
	UserType      = "user"
	ModeratorType = "moderator"

	ActiveStatus = "active"
	BannedStatus = "banned"
)

// TODO add validation
type User struct {
	ID               int64     `json:"id" valid:"int, optional"`
	VkID             int64     `json:"vk_id,omitempty" valid:"int, optional"`
	FirstName        string    `json:"first_name" valid:"utfletter, required"`
	SecondName       string    `json:"second_name" valid:"utfletter"`
	Email            string    `json:"login" valid:"email"`
	Password         string    `json:"password,omitempty" valid:"length(6|100)"`
	EncryptPassword  string    `json:"-" valid:"-"`
	Avatar           string    `json:"-" valid:"-"`
	Phones           []string  `json:"phones"`
	UserType         string    `json:"type" valid:"in(client|freelancer)"` // TODO: мы это не из пост запроса берем + неправильные переменные
	RegistrationDate time.Time `json:"registration_date" valid:"-"`
	LastUpdated      time.Time `json:"last_updated" valid:"-"`
	LastLogin        time.Time `json:"last_login" valid:"-"`
	City             int64     `json:"city"` // TODO: reformat into string
	Status           string    `json:"status"`
}

type SessionData struct {
	ID       int64
	VkID     int64
	UserType string
	City     int64
	Email    string
	Name     string
}

type UserFilter struct {
	Types  []string
	City   int64
	Status string
	Page   uint64
	Limit  uint64
}

func (u *User) BeforeCreate() error {
	u.UserType = UserType
	if len(u.Password) > 0 {
		enc, err := EncryptString(u.Password)
		if err != nil {
			return err
		}
		u.EncryptPassword = enc
	}

	u.Password = ""

	return nil
}

func EncryptString(s string) (string, error) {
	b, err := bcrypt.GenerateFromPassword([]byte(s), bcrypt.MinCost)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func (u *User) ComparePassword(password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(u.EncryptPassword), []byte(password)) == nil
}

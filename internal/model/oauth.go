package model

const (
	VKApi               = "https://api.vk.com/method"
	UserGetMethod       = "/users.get?v=5.52&fields=photo_400_orig&access_token="
	VkRedirectLocalHost = "http://localhost:9000/vk/auth/callback"
	VkRedirect          = "https://recyclepoints.ru/api/vk/auth/callback"
)

type VkUser struct {
	VkID        int64
	AccessToken string
	Email       string
	ExpiresIn   int64
	FirstName   string
	LastName    string
	Avatar      string
}

type VkResponseUserApi struct {
	Reponses []VkResponse `json:"response"`
}

type VkResponse struct {
	ID        float64 `json:"id"`
	FirstName string  `json:"first_name"`
	LastName  string  `json:"last_name"`
	Avatar    string  `json:"photo_400_orig"`
}

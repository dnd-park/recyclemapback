package model

type GeoPoint struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type City struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

package model

import "time"

type Group struct {
	ID   int64
	Name string
	City int
}

//TODO найти уже готовые нормлаьные геоджсоны
type GeoJson struct {
	Type        string     `json:"type"`
	Coordinates [2]float64 `json:"coordinates"`
}

const (
	StatusOnCheck   = "ON_CHECK"
	StatusApproved  = "APPROVED"
	StatusDenied    = "DENIED"
	StatusFrozen    = "FROZEN"
	StatusPublished = "PUBLISHED"
	StatusDeleted   = "DELETED"
)

type CollectionPoint struct {
	ID          int64      `json:"id"`
	PointID     int64      `json:"point_id,omitempty"`
	Title       string     `json:"title"`
	Description string     `json:"description,omitempty"`
	Coordinates *GeoJson   `json:"coordinates"`
	Latitude    float64    `json:"latitude,omitempty"`
	Longitude   float64    `json:"longitude,omitempty"`
	Address     string     `json:"address,omitempty"`
	City        int        `json:"city,omitempty"` // TODO: сделать строкой
	Sites       []string   `json:"sites,omitempty"`
	Phones      []string   `json:"phones,omitempty"`
	Emails      []string   `json:"emails,omitempty"`
	Images      []string   `json:"images,omitempty"`
	Tags        []string   `json:"tags,omitempty"`
	RubbishType []int64    `json:"fractions"`
	Group       int64      `json:"group,omitempty"`
	Status      string     `json:"status"`
	Available   bool       `json:"available"`
	CreatedBy   int64      `json:"createdBy,omitempty"` // TODO: сделать структурой и возвращать красиво
	UpdatedBy   int64      `json:"updatedBy,omitempty"` // TODO: сделать структурой и возвращать красиво
	UpdatedAt   time.Time  `json:"updatedAt,omitempty"`
	Timetable   *Timetable `json:"timetable,omitempty"`
	Time        string     `json:"timestring,omitempty"`
	AvgGrade    float64    `json:"grade"`
}

type PointsParams struct {
	IsFilter bool
	Filters  []int64
	IsAdmin  bool
	Page     uint64
	Limit    uint64
}

type FiltersReq struct {
	IsFilter  bool
	Filters   []int64
	Status    string
	CreatorID int64
	ID        int64
	City      int64
	Page      uint64
	Limit     uint64
}

package model

import "time"

type Complaint struct {
	ID          int64     `json:"id"`
	Author      int64     `json:"author"`
	Email       string    `json:"email"`
	Description string    `json:"description"`
	PointID     int64     `json:"point_id"`
	Type        string    `json:"type, omitempty"`
	ResponderID int64     `json:"responder_id, omitempty"`
	Response    string    `json:"response, omitempty"`
	IsAnswered  bool      `json:"is_answered, omitempty"`
	LastUpdated time.Time `json:"updated, omitempty" valid:"-"`
	Created     time.Time `json:"created, omitempty" valid:"-"`
}

type ComplaintParams struct {
	Answered    bool
	Email       string
	ID          int64
	Author      int64
	Page        uint64
	Limit       uint64
	PointID     uint64
	ResponderID uint64
}

type ComplaintAnswer struct {
	Answer string `json:"response"`
}

package main

import (
	apiserver "gitlab.com/dnd-park/recyclemapback/internal"
	"log"
)

func main() {
	if err := apiserver.Start(); err != nil {
		log.Fatal(err)
	}
}

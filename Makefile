.PHONY: build
build:
	go build ./cmd/service

.DEFAULT_GOAL := build
